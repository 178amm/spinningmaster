package com.dadrimon2.spinningmaster.controller.command.startup
{
	import com.dadrimon2.spinningmaster.model.proxy.*;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	/** 
	 * Prepare the Model.
	 */
	public class PrepareModelCommand extends SimpleCommand
	{
		override public function execute(note:INotification):void
		{
			// Create and register the Proxys for the domain model.
			var trackProxy:TrackProxy	 					= new TrackProxy();
			var sessionProxy:SessionProxy					= new SessionProxy();
			var workbenchTimerProxy:WorkbenchTimerProxy 	= new WorkbenchTimerProxy();

			// Register the proxy
			facade.registerProxy(trackProxy);
			facade.registerProxy(sessionProxy);
			facade.registerProxy(workbenchTimerProxy);
		}
	}
}