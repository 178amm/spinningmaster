package com.dadrimon2.spinningmaster.model.enum
{
	import mx.collections.ArrayCollection;
	
	public class TechniqueEnum //@TODO anyadir las que faltan
	{
		public static const LLANO:String 		= "Llano";
		public static const PENDIENTE:String 	= "Pendiente";
		
		public function TechniqueEnum()
		{
			
		}
		
		public static function get comboList():ArrayCollection
		{
			return (new ArrayCollection([LLANO, PENDIENTE]));
		}
	}
}