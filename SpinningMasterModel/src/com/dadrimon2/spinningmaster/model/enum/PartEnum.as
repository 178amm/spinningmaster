package com.dadrimon2.spinningmaster.model.enum
{
	import mx.collections.ArrayCollection;

	public class PartEnum //@TODO anyadir las que faltan
	{
		public static const ACONDICIONAMIENTO:String 	= "Acondicionamiento";
		public static const TRABAJO:String 				= "Trabajo";
			
		public function PartEnum()
		{
		}
		
		public static function get comboList():ArrayCollection
		{
			return (new ArrayCollection([ACONDICIONAMIENTO, TRABAJO]));
		}
	}
}