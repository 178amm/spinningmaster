package com.dadrimon2.spinningmaster.model.request
{
	import org.puremvc.as3.patterns.observer.Observer;
	
	// Hypothetical (Chapter 8 - Async Behavior)
	public class ServiceRequest extends Observer
	{
		public static const RESULT_OK:String = "result/ok";
		public static const RESULT_FAIL:String = "result/fail";
		
		public var hasCallback:Boolean = false;
		public var requestData:Object;
		public var resultData:Object;
		
		public function ServiceRequest( requestData:Object=null, 
										callback:Function=null, 
										caller:Object=null ) {
			// Store the Observer info
			super( callback, caller );
			
			// Store the request data
			this.requestData = requestData;
			
			// Remember whether complete Observer info was specified
			hasCallback = ( callback != null && caller != null );
		}
	}
}