package com.dadrimon2.spinningmaster.model.proxy
{
	import com.dadrimon2.utils.PTimer;
	
	import flash.events.Event;
	import flash.events.TimerEvent;
	
	import org.puremvc.as3.patterns.proxy.Proxy;
	
	public class WorkbenchTimerProxy extends Proxy
	{
		// Registered name
		public  static const NAME:String	= "WorkbenchTimerProxy";
		
		// Filesystem folder name for storage
		private static const FOLDER:String      = "workbenchTimer";
		
		// Default view update rate
		private static const K_UPDATE:Number	= 15;
		
		//Notification names
		public static const VIEW_UPDATER_TICK:String = NAME+"/"+FOLDER+"/viewUpdaterTick";
		
		private var viewUpdaterTimer:PTimer;
		private var trackTimer:PTimer;
		
		public function WorkbenchTimerProxy()
		{
			this.viewUpdaterTimer = new PTimer(K_UPDATE,0)
			this.trackTimer = new PTimer(0,1);
			
			this.viewUpdaterTimerTick = K_UPDATE;
			this.trackTimerTick = 0;
			
			//Add callbacks
			this.viewUpdaterTimer.onTick = this.onViewUpdaterTick;
			this.trackTimer.onTick = this.onTrackTick;
			
			super(NAME);
		}
		
		public function get trackTimerTick():Number
		{
			return (trackTimer.tick);
		}
		
		public function set trackTimerTick(value:Number):void
		{
			this.trackTimer.tick = value;
		}
		
		public function get viewUpdaterTimerTick():Number
		{
			return (viewUpdaterTimer.tick);
		}
		
		public function set viewUpdaterTimerTick(value:Number):void
		{
			this.viewUpdaterTimer.tick = value;
		}
		

		public function get trackTimerElapsed():Number
		{
			return (trackTimer.elapsed);
		}
		
		public function set trackTimerElapsed(value:Number)
		{
			trackTimer.elapsed = value;
		}

		public function get viewUpdaterTimerElapsed():Number
		{
			return this.viewUpdaterTimer.elapsed;
		}


		public function start():void
		{
			this.viewUpdaterTimer.start();
			this.trackTimer.start();
		}
		
		public function pause():void
		{
			this.viewUpdaterTimer.pause();
			this.trackTimer.pause();
		}
		
		public function stop():void
		{
			this.viewUpdaterTimer.stop();
			this.trackTimer.stop();
		}
		
		private function onViewUpdaterTick():void
		{
			//Notify the view
			var reached:Object = new Object();
			reached.currentValue = this.trackTimer.elapsed;
			reached.total = this.trackTimer.tick;
			sendNotification(VIEW_UPDATER_TICK, reached);
		}
		
		private function onTrackTick():void
		{
			//tell the view to stop
			//tell the music to stop?
		}
		
	}
}