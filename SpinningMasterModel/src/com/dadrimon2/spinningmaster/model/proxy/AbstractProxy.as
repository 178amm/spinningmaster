package com.dadrimon2.spinningmaster.model.proxy
{
	import com.dadrimon2.spinningmaster.model.vo.ValueObject;
	
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	
	import org.puremvc.as3.patterns.proxy.Proxy;
	
	public class AbstractProxy extends Proxy
	{
		// Extension for files
		private const FILE_EXT:String = ".xml";
		
		// Index filename
		private const INDEX:String    = "index"+FILE_EXT;
		
		// The folder to access. Set by connectToFolder()
		private var folder:File;
		
		public function AbstractProxy(proxyName:String=null, data:Object=null)
		{
			super(proxyName, data);
		}
		
		protected function connectToFolder( name:String ):void
		{
			folder = File.applicationStorageDirectory.resolvePath( name );
			if ( !folder.exists )
			{
				folder.createDirectory();
			} 
		}
		
		/**
		 * Write the value object's XML property to a file
		 * and update the index on disk and in memory.
		 */
		protected function writeVO(vo:ValueObject):void
		{
			var file:File = getFile(vo);
			
			trace ("Directory: " + File.applicationStorageDirectory.nativePath);
			
			writeFile(file, vo.xml);
		}
		
		protected function readVObyFileName(fileName:String):XML
		{
			var filename:String = "fileName" + FILE_EXT;
			return XML (folder.resolvePath(filename));
		}
		
		/**
		 * From the stub passed in, read the corresponding VO 
		 * from the disk and return the XML.
		 */
		protected function readVO( voStub:ValueObject ):XML
		{
			var file:File = getFile( voStub );
			return XML (readFile( file ) );
		}
		
		/**
		 * From the VO passed in, delete the corresponding VO
		 * from the disk and update the index.
		 */
		protected function deleteVO( vo:ValueObject ):void
		{
			var file:File = getFile( vo );
			if ( file.exists ) file.deleteFile(); 
		}
		
		/**
		 * Write xml to a file.
		 */
		private function writeFile(file:File, xml:XML):void
		{
			var stream:FileStream = new FileStream()
			stream.open(file, FileMode.WRITE);
			
			trace(xml.toString());
			stream.writeUTFBytes(xml.toXMLString());
			stream.close();
		}
		
		/**
		 * Read XML from a file and return it.
		 */
		private function readFile( file:File ):XML
		{
			var stream:FileStream = new FileStream()
			stream.open( file, FileMode.READ );	
			var xml:XML = XML( stream.readUTFBytes( stream.bytesAvailable ) );
			stream.close();
			return xml;
		}
		
		/**
		 * Get the corresponding File object for a given VO.
		 */
		private function getFile( vo:ValueObject ):File
		{
			//@TODO Remember to extract an unique ID in runtime
			//var filename:String = vo.uid + FILE_EXT;
			var filename:String = "cacheSessionProxy" + FILE_EXT;
			return folder.resolvePath( filename );
		}
		
	}
}