package com.dadrimon2.spinningmaster.model.proxy
{
	import com.dadrimon2.spinningmaster.model.vo.TrackVO;
	import com.hurlant.crypto.hash.MD5;
	import com.hurlant.util.Hex;
	
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.URLRequest;
	
	import mx.core.SoundAsset;
	
	import org.puremvc.as3.patterns.proxy.Proxy;
	
	public class TrackProxy extends Proxy
	{
		// Registered name
		public  static const NAME:String	= "TrackProxy";
		
		//Variables
		private var _auxFile:File;
		private var _trackSound:SoundAsset;
		private var _onAssetLoadedFunction:Function;
		
		//AuxVO, this is the only way to return a loaded TrackVO
		private var _loadedTrackVO:TrackVO;
				
		public function TrackProxy()
		{
			trace("llamo al constructor de trackproxy");
			_auxFile = new File();
			_trackSound = new SoundAsset();
			super(NAME);
		}
		
		/*
			GETTERS & SETTERS
		*/
		
		public function get loadedTrackVO():TrackVO
		{
			return _loadedTrackVO;
		}

		
		public function get trackSound():SoundAsset
		{
			return _trackSound;
		}
		
		public function set trackSound(value:SoundAsset):void
		{
			_trackSound = value;
		}

		
		/*
			INTERFACE
		*/
		
		//Load the track from a file and returns a VO 
		public function loadTrackFromFile(file:File, f:Function):void
		{
			this._auxFile = file;

			this._onAssetLoadedFunction = f;
			this._trackSound.addEventListener(Event.COMPLETE, onFileLoaded);
			this._trackSound.load(new URLRequest(file.url));
		}
		
		/*
			PRIVATE
		*/

		private function onFileLoaded(e:Event):void
		{
			var track:TrackVO = new TrackVO();
			
			//Fills the trackVO with the file information
			track.generateChecksum(this._auxFile);
			track.length = this._trackSound.length.toString();
			track.fullName = this._trackSound.url;
			
			this._loadedTrackVO = track;
			
			trace("Func: " + this._onAssetLoadedFunction);
			
			if (this._onAssetLoadedFunction != null)
			{
				trace ("Exec the function, VO loaded");
				this._onAssetLoadedFunction();
			}
		}
	}
}