package com.dadrimon2.spinningmaster.model.proxy
{
	import com.dadrimon2.spinningmaster.model.vo.SessionVO;
	
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	
	import mx.validators.StringValidator;

	public class SessionProxy extends AbstractProxy
	{
		public static const NAME:String 	= "SessionProxy";
		
		// Filesystem folder name for storage
		private static const FOLDER:String      = "session";
		
		// Notes sent from this Proxy
		public static const SESSION_CREATED:String = NAME+"/"+FOLDER+"/created";
		public static const SESSION_SAVED:String   = NAME+"/"+FOLDER+"/saved";
		public static const SESSION_DELETED:String = NAME+"/"+FOLDER+"/deleted";
		
		private var _file:File;
		private var _fileLoaded:Boolean;
		
		public function SessionProxy()
		{
			this._file = new File();
			this._fileLoaded = false;
			super(NAME);
		}
		
		public function get fileLoaded():Boolean
		{
			return _fileLoaded;
		}

		public function get file():File
		{
			return _file;
		}

		/**
		 * Called by the framework when the Proxy is registered.
		 */
		override public function onRegister():void
		{
			connectToFolder(FOLDER);
		}
		
		public function createSession(sessionName:String):SessionVO
		{
			var session:SessionVO = new SessionVO();
			
			//Assigns the name
			session.sessionName = sessionName;
			
			//Writes the vo
			this.writeVO(session);
			
			// Notify observers
			sendNotification(SESSION_CREATED, session);
			
			return (session);
		}
		
		public function loadSession(sessionName:String):SessionVO
		{
			
			var session:SessionVO = new SessionVO(this.readVObyFileName(sessionName));
			this._fileLoaded = true;
			
			return (session);
		}
		
		public function saveSession(session:SessionVO):void
		{
				this._fileLoaded = true;
				var stream:FileStream = new FileStream();
				stream.open(_file, FileMode.WRITE);
				stream.writeUTFBytes(session.xml.toXMLString());
				stream.close();	
		}
	}
}