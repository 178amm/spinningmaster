package com.dadrimon2.spinningmaster.model.vo
{
	[Bindable] public class TimeDataVO extends ValueObject
	{
		public static const ELEMENT:String = "timeData";
		
		public function TimeDataVO(xml:XML=null)
		{
			super(TimeDataVO.ELEMENT, xml);
		}
		
		public function set length(l:String):void
		{
			xml.setChildren(l);
		}
		
		public function get length():String
		{
			return ("");
		}
		
		public function set beginsAt(l:String):void
		{
			xml.@beginsAt = l;
		}
		
		public function get beginsAt():String
		{
			return (xml.@beginsAt);
		}
		
		public function set endsAt(l:String):void
		{
			xml.@endsAt = l;
		}
		
		public function get endsAt():String
		{
			return (xml.@endsAt);
		}
		
	}
}