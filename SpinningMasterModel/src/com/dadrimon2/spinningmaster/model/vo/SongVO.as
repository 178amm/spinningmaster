package com.dadrimon2.spinningmaster.model.vo
{
	[Bindable] public class SongVO extends ValueObject
	{
		public static const ELEMENT:String = "Song";
		
		public function SongVO(xml:XML=null)
		{
			super(SongVO.ELEMENT, xml);
		}
		
		public function set songName(l:String):void
		{
			xml.@songName = l;
		}
		
		public function get songName():String
		{
			return (xml.@songName);
		}
		
		public function set part(l:String):void
		{
			xml.@part = l;
		}
		
		public function get part():String
		{
			return (xml.@part);
		}
		
		public function set technique(l:String):void
		{
			xml.@technique = l;
		}
		
		public function get technique():String
		{
			return (xml.@technique);
		}
		
		public function set intensity(l:String):void
		{
			xml.@intensity = l;
		}
		
		public function get intensity():String
		{
			return (xml.@intensity);
		}
		
		public function set rpm(l:String):void
		{
			xml.@rpm = l;
		}
		
		public function get rpm():String
		{
			return (xml.@rpm);
		}
		
		public function set heightReached(l:String):void
		{
			xml.@heightReached = l;
		}
		
		public function get heightReached():String
		{
			return (xml.@heightReached);
		}
		
		public function set lengthTime(l:String):void
		{
			xml.@lengthTime = l;
		}
		
		public function get lengthTime():String
		{
			return (xml.@lengthTime);
		}
		
		public function set media(l:String):void
		{
			xml.@media = l;
		}
		
		public function get media():String
		{
			return (xml.@media);
		}
		
		public function equals(song:SongVO):Boolean
		{
			if (this.uid == song.uid)
			{
				return (true);
			}
			else
				return false;
		}
		
	}

}