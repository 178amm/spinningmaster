package com.dadrimon2.spinningmaster.model.vo
{
	[Bindable] public class SessionVO extends ValueObject
	{
		public static const ELEMENT:String = "session";
		
		private var _songTimeIndex:Vector.<Number>;
		
		public function SessionVO(xml:XML=null)
		{
			super(SessionVO.ELEMENT, xml);
			
			this._songTimeIndex = new Vector.<Number>;
			if (xml != null)
			{
				this.updateSongTimeIndex();
			}
		}
		
		/*
		Getters and Setters 
		*/
		override public function set xml( x:XML ):void
		{
			this.x = x;
			this.updateSongTimeIndex();
		}
		
		public function get songTimeIndex():Vector.<Number>
		{
			return _songTimeIndex;
		}

		public function set sessionName(l:String):void
		{
			xml.@sessionName = l;
		}
		
		public function get sessionName():String
		{
			return (xml.@sessionName);
		}
		
		public function get songs():Vector.<SongVO>
		{
			var v:Vector.<SongVO> = new Vector.<SongVO>();
			var xl:XMLList = this.xml.Song;
			
			for each (var sx:XML in xl)
			{
				var songvo:SongVO = new SongVO(sx);
				v.push(songvo);
			}
			
			return (v);
		}
		
		public function set songs(v:Vector.<SongVO>):void
		{
			delete this.xml.Song;
			
			for each (var e:SongVO in v)
			{
				this.xml.appendChild(e.xml);
			}
		}
		
		public function get track():TrackVO
		{
			var t:TrackVO;
			var xl:XMLList = this.xml.Track;
			
			if (xl.length()>0)
			{
				t = new TrackVO(xl[0]);
			}
			
			return (t);
		}
		
		public function set track(t:TrackVO):void
		{
			delete this.xml.Track;
			this.xml.appendChild(t.xml);
		}
		
		/*
		Interface
		*/
		
		public function addSong(song:SongVO):void
		{
			this.xml.appendChild(song.xml);
			this.updateSongTimeIndex();
		}
		
		
		public function removeSong(song:SongVO):Boolean
		{
			var removed:Boolean = false;
			var v:Vector.<SongVO> = new Vector.<SongVO>();
			var xl:XMLList = this.xml.song;
			
			for each (var s:SongVO in this.songs)
			{
				if (s.uid != song.uid)
				{
					v.push(s);
				}
			}
			
			if (v.length != this.songs.length)
			{
				this.songs = v;
				removed = true;
			}
			
			this.updateSongTimeIndex()
			
			return (removed);
		}
		
		public function getSongTimeBySong(song:SongVO):Number
		{
			var st:Number = 0;
			
			for (var i:int; i<this.songs.length; i++)
			{
				if (song.uid == this.songs[i].uid)
				{
					st = this._songTimeIndex[i];
				}
			}
			
			return (st);
		}
		
		public function getSongBySongTime(time:Number):SongVO
		{
			var sv:SongVO;
			
			for (var i:int; i<this._songTimeIndex.length; i++)
			{
				if (time > this._songTimeIndex[i])
				{
					sv = this.songs[i];
				}
			}
			
			return (sv);
		}
		
		private function updateSongTimeIndex():void
		{
			var v:Vector.<Number> = new Vector.<Number>;
			var initsAt:Number = 0;
			
			for each (var s:SongVO in this.songs)
			{
				v.push(initsAt);
				initsAt+=Number(s.lengthTime);
			}
			
			this._songTimeIndex = v;
		}
	}
}