package com.dadrimon2.spinningmaster.model.vo
{
	import com.hurlant.crypto.hash.MD5;
	import com.hurlant.util.Hex;

	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	
	public class TrackVO extends ValueObject
	{
		public static const ELEMENT:String = "Track";
		
		private var fileName:String;
		private var _checksum:String;	

		
		public function TrackVO(xml:XML=null)
		{
			super(TrackVO.ELEMENT, xml);
		}
		
		public function set fullName(l:String):void
		{
			xml.@fullName = String(l);
		}
		
		public function get fullName():String
		{
			return (xml.@fullName);
		}         
		
		public function set length(l:String):void
		{
			xml.@length = String(l);
		}
		
		public function get length():String
		{
			return (xml.@length);
		}         
		
		public function get checksum():String
		{
			return (xml.@checksum);
		}
		
		/*
			INTERFACE
		*/
		
		public function generateChecksum(f:File):void
		{
			var s:String = "";
			
			if (f != null)
			{
				//create a FileStream for the file
				var fileStream:FileStream = new FileStream();
				
				//open the file to read
				fileStream.open(f, FileMode.READ);
				
				//read the file into a string
				var chks:String = fileStream.readUTFBytes(fileStream.bytesAvailable);
				
				//close the file
				fileStream.close();
				
				var md5enc:MD5 = new MD5();
				s = Hex.fromArray(md5enc.hash(Hex.toArray(chks)));
				
				xml.@checksum = s;
			}
		}

		
		
	}
}