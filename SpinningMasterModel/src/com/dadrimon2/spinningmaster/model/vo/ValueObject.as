package com.dadrimon2.spinningmaster.model.vo
{
	public class ValueObject
	{
		import com.hurlant.crypto.hash.MD5;
		import com.hurlant.util.Hex;
		
		/**
		 * XML representation of Value Object. Override 
		 * accessors to provide default.
		 */
		public function set xml( x:XML ):void
		{
			this.x = x;
		}
		public function get xml():XML
		{
			return x;	
		}
		protected var x:XML;
		
		/**
		 * Unique identifier for this ValueObject
		 */
		public function set uid( u:String ):void
		{
			xml.@uid = u;
		}
		public function get uid():String
		{
			return String(xml.@uid);
		}
		
		
		public function ValueObject(elementName:String, xml:XML=null)
		{
			if (xml == null)
			{		
				//Encrypts the data
				this.x = <{elementName}></{elementName}>;
				
				if (ValueObject.debugMode == false)
				{
					var md5enc:MD5 = new MD5();
					var u:String = Hex.fromArray(md5enc.hash(Hex.toArray(elementName + new Date().time + uidRuntimeUniquifier++)));	
					this.uid = u;
				}
			}
			else
			{
				this.x = xml;
			}
			
		}
		
		/**
		 * Number of unique Value Objects created this runtime. 
		 * Used to ensure two objects of the same type created
		 * within the same millisecond will have a unique uid.
		 */
		protected static var uidRuntimeUniquifier:Number = 0;
		
		//Enables the debug mode 
		public static var debugMode:Boolean = false;
	}
}