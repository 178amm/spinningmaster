package com.dadrimon2.utils
{
	public class Parser
	{
		public static function millisecondsToDate(ms:String):Date
		{
			var d:Date = new Date(0,0,0,0,0,0,0);
			d.milliseconds+=Number(ms);
			
			return (d);
		}
		
		public static function padZero (str:String, digits:Number):String
		{
			if (str.length < digits)
				return padZero ("0" + str, digits)
			else
				return str;
		}
		
		public static function timeToMilliseconds(h:Number=0,m:Number=0, s:Number=0, ms:Number=0):Number
		{
			return ((h*60*60*1000)
					+ (m*60*1000)
					+ (s*1000)
					+ ms);
		}
		
		public static function millisecondsToHHMMSS(ms:String):String
		{
			var d:Date = Parser.millisecondsToDate(ms);
			
			return (Parser.padZero(d.hours.toString(), 2) + ":" +
				Parser.padZero(d.minutes.toString(), 2) + ":" +
				Parser.padZero(d.seconds.toString(), 2));
		}
	}
}