package com.dadrimon2.utils
{
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	import flash.utils.getTimer;
	
	public class PTimer
	{
		private var _auxTimer:Timer;
	
		//Default timer values
		private var _tick:Number;
		private var _nCount:Number;
		private var _running:Boolean;
		
		//Current values
		private var _elapsed:Number;
		private var _currentCount:Number;
		
		
		private var _startTime:Number; 
		private var _onTick:Function;
		
		public function PTimer(tick:Number, count:int)
		{
			this._auxTimer = new Timer(tick, 0);
			this._auxTimer.addEventListener(TimerEvent.TIMER, onEveryTick);
			
			this._tick = tick;
			this._nCount = count;
			this._running = false;
			
			this._elapsed = 0;
			this._currentCount = 0;
		}
		
		public function get tick():Number
		{
			return _tick;
		}
		
		public function set tick(value:Number):void
		{
			this._tick = value;
			this.stop();
		}

		//@TODO MODIFY THE COUNT
		
		public function get elapsed():Number
		{
			if (this._auxTimer.running)
			{
				return (this._elapsed + (getTimer()-_startTime));
			}
			else
			{
				return _elapsed;
			}
		}
		
		public function set elapsed(value:Number):void
		{
			if (this._auxTimer.running)
			{
				this.pause();
				this._elapsed = value;
			}
			else
			{
				this._elapsed = value;
				var e:Number = (this._tick - this._elapsed);
				if (e<0)
				{
					e=this._tick;
				}
				else
				{
					e=(this._tick - this._elapsed);
				}
				this._auxTimer.delay = e;
				this._auxTimer.stop();
			}
		}
		
		public function get currentCount():Number
		{
			return _currentCount;
		}


		
		//Callback setter for every tick
		public function set onTick(value:Function):void
		{
			_onTick = value;
		}

		//Starts the timer
		public function start():void
		{
			this._startTime = getTimer();
			this._auxTimer.start();	
			this._running = true;
		}
		
		public function pause():void
		{
			//If its runnig, perform the pause, else do nothing
			if (this._auxTimer.running)
			{
				//Sets elapsed time since the last pause
				this._elapsed += (getTimer()-_startTime);
				
				var e:Number = (this._tick - this._elapsed);
				if (e<0)
				{
					e=this._tick;
				}
				else
				{
					e=(this._tick - this._elapsed);
				}
				this._auxTimer.delay = e;
				this._auxTimer.stop();
				this._running = false;
			}
			else
			{
				trace ("Do nothing, isnt running");
			}
		}
		
		public function stop():void //Same as reset
		{
			this._auxTimer.stop();
			this._auxTimer.delay = this._tick;

			this._elapsed=0;
			this._currentCount = 0;
			this._running = false;
		}
		
		
		private function onEveryTick(te:TimerEvent):void
		{
			this._currentCount++;
			
			if (this._currentCount == this._nCount)
			{
				this.stop(); //Resets the time
			}
			else
			{
				this._auxTimer.delay = this._tick;
				this._elapsed = 0;
				this._startTime = getTimer();
				
				if (this._onTick!= null)
				{
					this._onTick();
				}
			}
		}
	}
}