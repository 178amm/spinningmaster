package com.andrevenancio.audio
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.geom.ColorTransform;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundMixer;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import flash.utils.ByteArray;
	import flash.utils.Timer;
 
	public class BeatDetection extends Sprite
	{
		private var _channel:SoundChannel;
		private var _sound:Sound;
		private var _timer:Timer;
		private var _link:String;
 
		private var _play:Sprite = new Sprite();
		private var spectrumBars_mc:Sprite = new Sprite();
		private var beatMinimum_mc:Sprite = new Sprite();
		private var averageMagnitude_mc:Sprite	= new Sprite();
		private var currentMagnitude_mc:Sprite	= new Sprite();
		private var beat:Sprite = new Sprite();
 
		private var channels:Number = 256;
		private var ba:ByteArray = new ByteArray();
		private var media:Array;
		private var valor:int;
 
		private var maxUpLimit:uint = 200;
		private var multiplier:uint = 100;
		private var startRange:uint = 0;
		private var stopRange:uint = 128;
		//this value changes from song to song. 
		private var factor:uint = 64;
 
		public function BeatDetection(_mp3:String)
		{		
			_link = _mp3;
			config();
		}
 
		private function config():void
		{		
			var _text:TextField = new TextField()
			_text.text = "click here to play";
			_text.autoSize = "left";
			_text.selectable = _text.mouseEnabled = false;
			_text.textColor = 0xFFFFFF;
			_play.addChild(_text);
			addChild(_play);
 
			_play.x = 400/2 - _play.width/2;
			_play.y = 300/2 - _play.height/2;
 
			_play.graphics.beginFill(0x336699);
			_play.graphics.drawRect(0,0, _text.width, _text.height);
			_play.graphics.endFill()
			_play.buttonMode = true;
 
			_play.addEventListener(MouseEvent.CLICK, doPlay);			
		}
 
		private function doPlay(e:MouseEvent):void
		{
			_play.removeEventListener(MouseEvent.CLICK, doPlay);
			removeChild(_play);
 
			//create visuals
			addChild(beat);
			beat.alpha = 0;
			beat.graphics.beginFill(0xFF0000);
			beat.graphics.drawRect(0, 0, 400, 300)
			beat.graphics.endFill();
 
			addChild(spectrumBars_mc);
			spectrumBars_mc.x = (400-256) / 2;
			spectrumBars_mc.y = 200;
			spectrumBars_mc.addChild(beatMinimum_mc)
			spectrumBars_mc.addChild(averageMagnitude_mc);
			spectrumBars_mc.addChild(currentMagnitude_mc);
 
			addBar("Treshold", 0xFF0000, beatMinimum_mc);
			addBar("Average amplitude", 0x00CC00, averageMagnitude_mc);
			addBar("Current amplitude", 0x000099, currentMagnitude_mc);
 
			//loads sound
			_sound = new Sound(new URLRequest(_link));
			_sound.addEventListener(Event.COMPLETE, loadComplete);
		}
 
		private function addBar(_title:String, _color:int, _targetSP:Sprite):void
		{
			_targetSP.graphics.beginFill(0x000000);
			_targetSP.graphics.drawRect(0, 0, channels, 1);
			_targetSP.graphics.endFill();
 
			var colorTransform:ColorTransform = _targetSP.transform.colorTransform;
				colorTransform.color = _color;
 
			_targetSP.transform.colorTransform = colorTransform;
		}
 
		private function loadComplete(e:Event):void
		{
			_sound.removeEventListener(Event.COMPLETE, loadComplete);
 
			_sound = e.target as Sound;
			_channel = _sound.play();
			_channel.addEventListener(Event.SOUND_COMPLETE, reStart);
			_sound.play();
 
			//starts timer for beat detection
			_timer = new Timer(26);
			_timer.addEventListener(TimerEvent.TIMER, getCurrentMagnitude);
			_timer.start();
		}
 
		public function reStart(e:Event):void
		{
			_timer.stop();
			_timer = null;
 
			_channel.stop();
			_sound = null;
			removeChild(spectrumBars_mc);
 
			config();
		}
 
		private function getCurrentMagnitude(e:TimerEvent):void
		{
			SoundMixer.computeSpectrum(ba,true,0);
			spectrumBars_mc.graphics.clear();
			media = new Array();
 
			for (var i:int=0; i = 1)
			{
				var aLength:uint = media.length;
				var average:uint = 0;
				var max:uint = 0;
				var med:uint = 0;
 
				for (var i:uint = 0; i max?media[i]:max;
					}
				}
 
				currentMagnitude_mc.y =- max;
				med = (max / 2);
				average = average/aLength;
				averageMagnitude_mc.y = -med;
				beatMinimum_mc.y = -(med+factor);
 
				if (currentMagnitude_mc.y<beatMinimum_mc.y)
				{
					//WE DETECTED A BEAT!!
					beat.alpha=1;
				}
				else
				{
					beat.alpha=0;
				}
			}
		}
	}
}