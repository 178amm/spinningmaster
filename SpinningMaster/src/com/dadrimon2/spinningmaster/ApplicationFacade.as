package com.dadrimon2.spinningmaster
{
	import org.puremvc.as3.patterns.facade.Facade;
	
	import com.dadrimon2.spinningmaster.controller.constant.AppConstants;
	import com.dadrimon2.spinningmaster.controller.command.startup.StartupCommand;
	
	public class ApplicationFacade extends Facade
	{
		/**
		 * The singleton instance factory method.
		 */
		public static function getInstance() : ApplicationFacade 
		{
			if (instance == null) instance = new ApplicationFacade();
			return instance as ApplicationFacade;
		}
		
		/**
		 * A convenience method for starting up the PureMVC 
		 * apparatus from the application.
		 */
		public function startup(app:SpinningMaster):void
		{
			registerCommand(AppConstants.STARTUP, StartupCommand);
			sendNotification(AppConstants.STARTUP, app);
		}
	}
}