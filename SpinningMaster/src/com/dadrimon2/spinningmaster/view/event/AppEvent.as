package com.dadrimon2.spinningmaster.view.event
{
	import flash.events.Event;
	
	public class AppEvent extends Event
	{
		public var data:Object;		// optional data object
		public var related:Object;  // optional related data object
		
		//Verbs
		private static const ADD:String 			= "add";
		private static const EDIT:String			= "edit";
		private static const CLOSE:String			= "close";
		private static const CREATE:String			= "create";
		private static const SWITCH:String			= "switch";
		private static const NEW:String				= "new";
		private static const TOGGLE:String			= "toggle";
		private static const SAVE:String			= "save"
		private static const LOAD:String			= "load"
		private static const SHOW:String			= "show"
		private static const EXIT:String			= "exit"
		private static const CHANGE:String			= "change"
		private static const UPDATE:String			= "update"
		private static const DELETE:String			= "delete"
		private static const PLAY:String			= "play"
		private static const PAUSE:String			= "pause"
		private static const STOP:String			= "stop"
		private static const CHANGED:String			= "changed"
		
		//Nouns
		private static const WORKBENCH:String 		= "Workbench";
		private static const PLAYER:String 			= "Player";
		private static const EDITOR:String 			= "Editor";
		private static const SESSION:String			= "Session";
		private static const MODE:String			= "Mode";
		private static const APP:String				= "App";
		private static const PREFERENCES:String		= "Preferences";
		private static const ABOUT:String			= "About";
		private static const TRACK:String			= "Track";
		private static const EDITION:String			= "Edition";
		private static const RPM:String				= "RPM";
		private static const TECHNIQUE:String		= "Techniqe";
		private static const PART:String			= "Part";
		private static const INTENSITY:String		= "Intensity";
		private static const SONG:String			= "Song";
		private static const LIST:String			= "List";
		private static const HEIGHT:String			= "Height";
		private static const VIDEO:String			= "Video";
		private static const STATE:String			= "State";
		private static const MEDIA:String			= "Media";
		private static const SEEK:String			= "Seek";
		private static const FULLSCREENMODE:String	= "FullScreenMode";
		
		//Main menu
		public static const NEW_SESSION:String			= NEW+SESSION;
		public static const OPEN_SESSION:String			= OPEN+SESSION;
		public static const SAVE_SESSION:String			= SAVE+SESSION;
		public static const CLOSE_SESSION:String		= CLOSE+SESSION;
		public static const PLAY_SESSION:String			= PLAY+SESSION;
		public static const PAUSE_SESSION:String		= PAUSE+SESSION;
		public static const STOP_SESSION:String			= STOP+SESSION;
		public static const SEEK_SESSION:String			= SEEK+SESSION;
		
		public static const TOGGLE_FULLSCREEN_MODE:String = TOGGLE+FULLSCREEN+MODE;
		
		
		public static const LOAD_TRACK:String			= LOAD+TRACK;
		public static const EXIT_APP:String				= EXIT+APP;
		public static const SHOW_PREFERENCES:String		= SHOW+PREFERENCES;
		public static const TOGGLE_EDITION_MODE:String	= TOGGLE+EDITION+MODE;
		public static const SHOW_ABOUT:String			= SHOW+ABOUT;
		public static const TOGGLE_FULLSCREENMODE:String= TOGGLE+FULLSCREENMODE;
		
		//Data editor
		public static const CHANGE_RPM:String			= CHANGE+RPM;
		public static const CHANGE_TECHNIQUE:String		= CHANGE+TECHNIQUE;
		public static const CHANGE_PART:String			= CHANGE+PART;
		public static const CHANGE_INTENSITY:String		= CHANGE+INTENSITY;
		public static const CHANGE_HEIGHT:String		= CHANGE+HEIGHT;
		public static const UPDATE_SONG_LIST:String		= UPDATE+SONG+LIST;
		
		public static const ADD_SONG:String				= ADD+SONG;
		public static const DELETE_SONG:String			= DELETE+SONG;
		public static const SELECT_SONG:String			= SELECT+SONG;
		public static const SAVE_SONG:String			= SAVE+SONG;
		public static const VIDEO_STATE_CHANGED:String	= VIDEO+STATE+CHANGED;
		public static const LOAD_MEDIA:String			= LOAD+MEDIA;
		
		
		
		public function AppEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			
			this.data = data;
			this.related = related;
		}
	}
}