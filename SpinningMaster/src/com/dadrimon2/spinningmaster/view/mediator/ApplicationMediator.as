package com.dadrimon2.spinningmaster.view.mediator
{
	import com.dadrimon2.spinningmaster.controller.constant.AppConstants;
	import com.dadrimon2.spinningmaster.model.vo.SessionVO;
	import com.dadrimon2.spinningmaster.view.context.SelectionContext;
	import com.dadrimon2.spinningmaster.view.event.AppEvent;
	
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.media.SoundChannel;
	import flash.net.URLRequest;
	
	import mx.core.SoundAsset;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.mediator.Mediator;
	import org.puremvc.as3.patterns.observer.Notification;
	
	public class ApplicationMediator extends Mediator
	{
		public static const NAME:String = "ApplicationMediator";
		
		public function ApplicationMediator(app:SpinningMaster)
		{
			super(ApplicationMediator.NAME, app);
		}
		
		override public function onRegister():void
		{
			// Set the initial display mode
			app.setMode(SpinningMaster.MODE_WELCOME)
				
			// Set the SelectionContext
			var scProxy:IProxy = facade.retrieveProxy(SelectionContext.NAME);
			app.context = SelectionContext(scProxy.getData());
			
			
			//Add the event listener to the view
			app.addEventListener(AppEvent.NEW_SESSION, handleAppEvent);
			app.addEventListener(AppEvent.OPEN_SESSION, handleAppEvent);
			app.addEventListener(AppEvent.SAVE_SESSION, handleAppEvent);
			app.addEventListener(AppEvent.CLOSE_SESSION, handleAppEvent);
			app.addEventListener(AppEvent.EXIT_APP, handleAppEvent);
			app.addEventListener(AppEvent.LOAD_TRACK, handleAppEvent);
			app.addEventListener(AppEvent.SHOW_PREFERENCES, handleAppEvent);
			app.addEventListener(AppEvent.TOGGLE_EDITION_MODE, handleAppEvent);
			app.addEventListener(AppEvent.SHOW_ABOUT, handleAppEvent);
			app.addEventListener(AppEvent.TOGGLE_FULLSCREENMODE, handleAppEvent);

		}

		
		
		override public function listNotificationInterests():Array
		{
			return [
					//From the controller
					AppConstants.SHOW_WELCOME,
					AppConstants.SHOW_WORKBENCH,
					AppConstants.CLOSE_APP,
					AppConstants.SET_FULLSCREENMODE
	
			];			
		}		
		
		/**
		 * Handle the notifications this Mediator is interested in.
		 */
		override public function handleNotification(note:INotification):void
		{
			switch (note.getName()) 
			{
				case AppConstants.SHOW_WELCOME:
					app.setMode(SpinningMaster.MODE_WELCOME);
					break;
				case AppConstants.SHOW_WORKBENCH:
					app.setMode(SpinningMaster.MODE_WORKBENCH);
					break;
				case AppConstants.CLOSE_APP:
					app.exitApp();
					break;
				case AppConstants.SET_FULLSCREENMODE:
					app.setFullScreenMode(note.getBody() as String);
					break;
			}
		}
		
		/**
		 * Handle AppEvents from the view component.
		 */
		private function handleAppEvent( event:AppEvent ):void 
		{
			switch (event.type) 
			{
				//File
				case AppEvent.NEW_SESSION:
					sendNotification(AppConstants.NEW_SESSION, app.getMode());
					break;
				case AppEvent.OPEN_SESSION:
					sendNotification(AppConstants.LOAD_SESSION, app.getMode());
					break;
				case AppEvent.SAVE_SESSION:
					sendNotification(AppConstants.SAVE_SESSION);
					break;
				case AppEvent.CLOSE_SESSION:
					sendNotification(AppConstants.CLOSE_SESSION);
					break;
				case AppEvent.EXIT_APP:
					sendNotification(AppConstants.CLOSE_APP);
					break;
				
				//Edition
				case AppEvent.LOAD_TRACK:
					sendNotification(AppConstants.BROWSE_LOAD_TRACK);
					break;
				case AppEvent.SHOW_PREFERENCES:
					//sendNotification(AppConstants.LOAD_TRACK);
					break;
				case AppEvent.TOGGLE_EDITION_MODE:
					sendNotification(AppConstants.SET_WORKBENCH_MODE,"");
					break;
				
				//Watch
				case AppEvent.TOGGLE_FULLSCREENMODE:
					sendNotification(AppConstants.TOGGLE_FULLSCREENMODE, app.getFullScreenMode());
					break;
				
				//About
				case AppEvent.SHOW_ABOUT:
					//sendNotification(AppConstants.CLOSE_SESSION);
					break;
			}
		}
		
		/**
		 * Cast the view component to the correct type;
		 */
		private function get app():SpinningMaster
		{
			return viewComponent as SpinningMaster;
		}
	}
}