package com.dadrimon2.spinningmaster.view.mediator
{	
	import com.dadrimon2.spinningmaster.controller.constant.AppConstants;
	import com.dadrimon2.spinningmaster.model.proxy.WorkbenchTimerProxy;
	import com.dadrimon2.spinningmaster.model.vo.SessionVO;
	import com.dadrimon2.spinningmaster.model.vo.SongVO;
	import com.dadrimon2.spinningmaster.view.component.workbench.Workbench;
	import com.dadrimon2.spinningmaster.view.component.workbench.music.MusicPlayer;
	import com.dadrimon2.spinningmaster.view.component.workbench.track.TrackData;
	import com.dadrimon2.spinningmaster.view.context.SelectionContext;
	import com.dadrimon2.spinningmaster.view.event.AppEvent;
	import com.dadrimon2.utils.Parser;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.mediator.Mediator;

	public class WorkbenchMediator extends Mediator
	{
		public static const NAME:String = "WorkbenchMediator";
		
		public function WorkbenchMediator(workbench:Workbench)
		{
			super(WorkbenchMediator.NAME, workbench);
		}
		
		override public function onRegister():void
		{
			// Set the initial display mode
			workbench.setMode(Workbench.MODE_PLAYER);
			workbench.musicPlayer.status = MusicPlayer.PLAYER_DISABLED;
			workbench.trackData.setMode(TrackData.PLAYER_DISABLED);
			
			// Set the SelectionContext
			var scProxy:IProxy = facade.retrieveProxy(SelectionContext.NAME);
			workbench.player.dataDisplay.context = SelectionContext(scProxy.getData());
			workbench.editor.dataEditor.context = SelectionContext(scProxy.getData());
			workbench.profileChart.context = SelectionContext(scProxy.getData());
			workbench.heartChart.context = SelectionContext(scProxy.getData());
			
			
			//Add the event listeners to the view
			workbench.editor.dataEditor.addEventListener(AppEvent.ADD_SONG, handleAppEvent);
			workbench.editor.dataEditor.addEventListener(AppEvent.DELETE_SONG, handleAppEvent);
			workbench.editor.dataEditor.addEventListener(AppEvent.SAVE_SONG, handleAppEvent);
			workbench.editor.dataEditor.addEventListener(AppEvent.SELECT_SONG, handleAppEvent);
			workbench.editor.dataEditor.addEventListener(AppEvent.NEW_SESSION, handleAppEvent);
			workbench.editor.dataEditor.addEventListener(AppEvent.CHANGE_HEIGHT, handleAppEvent);
			workbench.editor.dataEditor.addEventListener(AppEvent.LOAD_MEDIA, handleAppEvent);
			
			workbench.musicPlayer.addEventListener(AppEvent.PLAY_SESSION, handleAppEvent);
			workbench.musicPlayer.addEventListener(AppEvent.PAUSE_SESSION, handleAppEvent);
			workbench.musicPlayer.addEventListener(AppEvent.STOP_SESSION, handleAppEvent);
			workbench.musicPlayer.addEventListener(AppEvent.SEEK_SESSION, handleAppEvent);
			
			//Video/Media management
			//workbench.vid
			workbench.videoPlayer.addEventListener(AppEvent.VIDEO_STATE_CHANGED, handleAppEvent);
			
			//Clock init
			workbench.player.clock.init();
			workbench.profileChart.init();
		}
		
		override public function listNotificationInterests():Array
		{
			return [ 
				//From the controller
				AppConstants.SET_WORKBENCH_MODE,
				AppConstants.UPDATE_SONG_INDEX,
				AppConstants.UPDATE_SONG_DATA,
				AppConstants.SELECT_SONG_INDEX,
				AppConstants.TRACK_LOADED,
				AppConstants.CHANGE_VIDEO_SOURCE,
				AppConstants.CHANGE_VIDEO_TIME,
				//From the model
				WorkbenchTimerProxy.VIEW_UPDATER_TICK		
			];			
		}		
		
		/**
		 * Handle the notifications this Mediator is interested in.
		 */
		override public function handleNotification(note:INotification):void
		{
			switch (note.getName()) 
			{
				case AppConstants.SET_WORKBENCH_MODE:
					workbench.setMode(note.getBody().toString());
					break;
				case AppConstants.UPDATE_SONG_INDEX:
					workbench.editor.dataEditor.updateSongIndex(note.getBody() as SessionVO);
					workbench.profileChart.updateChart(note.getBody() as SessionVO);
					workbench.heartChart.updateChart(note.getBody() as SessionVO);
					break;
				case AppConstants.SELECT_SONG_INDEX: 
					workbench.editor.dataEditor.selectSongIndex(note.getBody() as Number);
					break;
				case AppConstants.UPDATE_SONG_DATA: //Song changed
					workbench.editor.dataEditor.updateSongData(SongVO(note.getBody()));
					workbench.player.dataDisplay.updateSongData(SongVO(note.getBody()));
					workbench.changeVideoSource(SongVO(note.getBody()).media);
					workbench.editor.dataEditor.mediaURLLabel.text = String(note.getBody()).substr(0,15) + "..." +  String(note.getBody()).substr(-10);
					break;
				case WorkbenchTimerProxy.VIEW_UPDATER_TICK:
					var reached:Object = note.getBody();
					reached.lastValue = workbench.player.dataDisplay.context.lastTimeUpdate;
					//workbench.player.dataDisplay.checkForNewSong(reached.currentValue);
					sendNotification(AppConstants.CHECK_NEW_SONG, reached);
					
					//Drawing issues
					workbench.musicPlayer.setCurrentAndLetfTimeLabel(reached.currentValue, Number(workbench.editor.dataEditor.context.sessionVO.track.length));
					workbench.musicPlayer.musicSlider.value = reached.currentValue;
					workbench.player.dataDisplay.updateSongTimeLeft(reached.currentValue);
					workbench.profileChart.drawCursor(reached.currentValue);
					workbench.heartChart.drawCursor(reached.currentValue);
					
					workbench.player.dataDisplay.context.lastTimeUpdate = reached.currentValue;
					break;
				case AppConstants.TRACK_LOADED:
					workbench.trackData.setMode(TrackData.PLAYER_ENABLED, workbench.editor.dataEditor.context.sessionVO.track);
					workbench.profileChart.dateAxisMaxLength = workbench.editor.dataEditor.context.sessionVO.track.length;
					workbench.heartChart.dateAxisMaxLength = workbench.editor.dataEditor.context.sessionVO.track.length;
					workbench.musicPlayer.musicSlider.maximum = Number(workbench.editor.dataEditor.context.sessionVO.track.length);
					workbench.musicPlayer.status = MusicPlayer.PLAYER_ENABLED;
					break;
				case AppConstants.CHANGE_VIDEO_SOURCE:
					workbench.changeVideoSource(note.getBody());
					workbench.editor.dataEditor.mediaURLLabel.text = String(note.getBody()).substr(0,15) + "..." +  String(note.getBody()).substr(-10);
					break;
				case AppConstants.CHANGE_VIDEO_TIME:
					trace ("Change video time: " +  Number(note.getBody()));
					workbench.changeVideoStartAt(Number(note.getBody()));
					break;
					
			}
		}
		
		/**
		 * Handle AppEvents from the view component.
		 */
		private function handleAppEvent(event:AppEvent):void 
		{
			switch (event.type) 
			{
				case AppEvent.ADD_SONG:
					sendNotification(AppConstants.ADD_SONG, workbench.musicPlayer.musicSlider.value);
					break;
				case AppEvent.DELETE_SONG:
					sendNotification(AppConstants.DELETE_SONG, event.data);
					break;
				case AppEvent.SELECT_SONG:
					sendNotification(AppConstants.SELECT_SONG, event.data);
					break;
				case AppEvent.SAVE_SONG:
					sendNotification(AppConstants.SAVE_SONG, event.data);
					break;
				case AppEvent.CHANGE_HEIGHT: //this must be named smth like UPDATE_CURRENT_SONG
					sendNotification(AppConstants.SAVE_SONG, event.data);
					sendNotification(AppConstants.UPDATE_SONG_INDEX);
					break;
				
				case AppEvent.PLAY_SESSION:
					workbench.videoPlayer.play(); //TODO cambiar, es una marranada
					sendNotification(AppConstants.PLAY_SESSION, event.data);
					break;
				case AppEvent.PAUSE_SESSION:
					workbench.videoPlayer.pause(); //TODO cambiar, es una marranada
					sendNotification(AppConstants.PAUSE_SESSION, event.data);
					break;
				case AppEvent.STOP_SESSION:
					workbench.videoPlayer.stop(); //TODO cambiar, es una marranada
					sendNotification(AppConstants.STOP_SESSION, event.data);
					break;
				case AppEvent.SEEK_SESSION:
					sendNotification(AppConstants.SEEK_SESSION, event.data);
					break;
				
				case AppEvent.LOAD_MEDIA:
					sendNotification(AppConstants.LOAD_MEDIA, event.data);
					break;
				case AppEvent.VIDEO_STATE_CHANGED:
					//sendNotification(AppConstants.STOP_SESSION, event.data);
					break;
			}
		}
		
		/**
		 * Cast the view component to the correct type;
		 */
		private function get workbench():Workbench
		{
			return viewComponent as Workbench;
		}
	}
}