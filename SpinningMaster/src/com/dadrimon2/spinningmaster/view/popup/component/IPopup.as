package com.dadrimon2.spinningmaster.view.popup.component
{
	import mx.core.IFlexDisplayObject;

	/**
	 * Interface for popups
	 */
	public interface IPopup extends IFlexDisplayObject
	{
		function setData( data:Object ):void;
		function getEvents():Array;
	}
}