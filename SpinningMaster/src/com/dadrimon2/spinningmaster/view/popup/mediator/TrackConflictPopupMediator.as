package com.dadrimon2.spinningmaster.view.popup.mediator
{
	import com.dadrimon2.spinningmaster.view.popup.component.AddSongPopup;
	import com.dadrimon2.spinningmaster.view.popup.component.IPopup;
	import com.dadrimon2.spinningmaster.view.popup.component.TrackConflictPopup;
	import com.dadrimon2.spinningmaster.view.popup.request.PopupRequest;
	
	public class TrackConflictPopupMediator extends AbstractPopupMediator
	{
		public static const NAME:String = "TrackConflictPopupMediator";
		
		public function TrackConflictPopupMediator()
		{
			super(NAME);
		}
		
		override public function listNotificationInterests():Array
		{
			return [PopupRequest.TRACK_CONFLICT_POPUP];
		}

		override protected function popupFactory():IPopup
		{
			return new TrackConflictPopup();
		}
		
	}
}