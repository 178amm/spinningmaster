package com.dadrimon2.spinningmaster.view.popup.mediator
{
	import com.dadrimon2.spinningmaster.view.popup.component.IPopup;
	import com.dadrimon2.spinningmaster.view.popup.component.DeleteSongPopup;
	import com.dadrimon2.spinningmaster.view.popup.request.PopupRequest;
	
	public class DeleteSongPopupMediator extends AbstractPopupMediator
	{
		public static const NAME:String = "DeleteSongPopupMediator";
		
		public function DeleteSongPopupMediator()
		{
			super(NAME);
		}
		
		override public function listNotificationInterests():Array
		{
			return [PopupRequest.DELETE_SONG_POPUP];
		}

		override protected function popupFactory():IPopup
		{
			return new DeleteSongPopup();
		}
		
	}
}