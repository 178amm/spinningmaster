package com.dadrimon2.spinningmaster.view.popup.mediator
{
	import com.dadrimon2.spinningmaster.view.popup.component.AddSongPopup;
	import com.dadrimon2.spinningmaster.view.popup.component.IPopup;
	import com.dadrimon2.spinningmaster.view.popup.request.PopupRequest;
	
	public class AddSongPopupMediator extends AbstractPopupMediator
	{
		public static const NAME:String = "AddSongPopupMediator";
		
		public function AddSongPopupMediator()
		{
			super(NAME);
		}
		
		override public function onRegister():void
		{

		}
		
		override public function listNotificationInterests():Array
		{
			return [PopupRequest.ADD_SONG_POPUP];
		}

		override protected function popupFactory():IPopup
		{
			var songPopup:AddSongPopup = new AddSongPopup();
			songPopup.recommendedLength = (this.request.data.recommendedLength) as Date;

			return (songPopup);
		}
		
	}
}