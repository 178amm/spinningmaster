package com.dadrimon2.spinningmaster.view.popup.mediator
{
	import com.dadrimon2.spinningmaster.view.popup.component.IPopup;
	import com.dadrimon2.spinningmaster.view.popup.component.AboutPopup;
	import com.dadrimon2.spinningmaster.view.popup.request.PopupRequest;
	
	public class AboutPopupMediator extends AbstractPopupMediator
	{
		public static const NAME:String = "AboutPopupMediator";
		
		public function AboutPopupMediator()
		{
			super(NAME);
		}
		
		override public function listNotificationInterests():Array
		{
			return [PopupRequest.ABOUT_POPUP];
		}

		override protected function popupFactory():IPopup
		{
			return new AboutPopup();
		}
		
	}
}