package com.dadrimon2.spinningmaster.view.popup.mediator
{
	import com.dadrimon2.spinningmaster.view.popup.component.AddSongPopup;
	import com.dadrimon2.spinningmaster.view.popup.component.IPopup;
	import com.dadrimon2.spinningmaster.view.popup.component.SaveSessionPopup;
	import com.dadrimon2.spinningmaster.view.popup.request.PopupRequest;
	
	public class SaveSessionPopupMediator extends AbstractPopupMediator
	{
		public static const NAME:String = "SaveSessionPopupMediator";
		
		public function SaveSessionPopupMediator()
		{
			super(NAME);
		}
		
		override public function listNotificationInterests():Array
		{
			return [PopupRequest.SAVE_SESSION_POPUP];
		}

		override protected function popupFactory():IPopup
		{
			return new SaveSessionPopup();
		}
		
	}
}