package com.dadrimon2.spinningmaster.view.context
{
	import com.dadrimon2.spinningmaster.model.vo.SessionVO;
	import com.dadrimon2.spinningmaster.model.vo.TrackVO;
	
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.media.SoundChannel;
	
	import mx.core.SoundAsset;

	[Bindable] public class SelectionContext
	{
		public static const NAME:String = "SelectionContext";

		public var sessionVO:SessionVO;
		public var trackVO:TrackVO;
		public var lastTimeUpdate:Number;
		
		private var soundChannel:SoundChannel;
		
		public function SelectionContext()
		{
			this.sessionVO = new SessionVO();
			this.trackVO = new TrackVO();
			this.lastTimeUpdate = 0;
		}
		
		//Play the track at certain point
		public function playSong(sound:SoundAsset, startAt:Number):void
		{
			this.soundChannel = sound.play(startAt);
		}
		
		//Stop the track
		public function stopSong():void
		{
			if (this.soundChannel != null)
			{
				this.soundChannel.stop();	
			}
		}
		
		//Resets the context
		public function reset():void
		{
			this.sessionVO = null;
			this.trackVO = null;
			
			this.sessionVO = new SessionVO();
			this.trackVO = new TrackVO();
			this.lastTimeUpdate = 0;
		}
	}
}