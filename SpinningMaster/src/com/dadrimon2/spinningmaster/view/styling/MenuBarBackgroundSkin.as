

////////////////////////////////////////////////////////////////////////////////
//
//  ADOBE SYSTEMS INCORPORATED
//  Copyright 2005-2006 Adobe Systems Incorporated
//  All Rights Reserved.
//
//  NOTICE: Adobe permits you to use, modify, and distribute this file
//  in accordance with the terms of the license agreement accompanying it.
//
////////////////////////////////////////////////////////////////////////////////

package com.dadrimon2.spinningmaster.view.styling
{
	
	import flash.display.GradientType;
	
	import mx.core.EdgeMetrics;
	import mx.core.UIComponent;
	import mx.skins.Border;
	import mx.styles.StyleManager;
	import mx.utils.ColorUtil;
	
	/**
	 *  The skin for the background of a MenuBar.
	 *  
	 *  @langversion 3.0
	 *  @playerversion Flash 9
	 *  @playerversion AIR 1.1
	 *  @productversion Flex 3
	 */
	public class MenuBarBackgroundSkin extends Border
	{
		//--------------------------------------------------------------------------
		//
		//  Class variables
		//
		//--------------------------------------------------------------------------
	
		//--------------------------------------------------------------------------
		//
		//  Class methods
		//
		//--------------------------------------------------------------------------
	
		
		//--------------------------------------------------------------------------
		//
		//  Constructor
		//
		//--------------------------------------------------------------------------
	
		/**
		 *  Constructor.
		 *  
		 *  @langversion 3.0
		 *  @playerversion Flash 9
		 *  @playerversion AIR 1.1
		 *  @productversion Flex 3
		 */
		public function MenuBarBackgroundSkin()
		{
			super();
		}
		
		//--------------------------------------------------------------------------
		//
		//  Overridden properties
		//
		//--------------------------------------------------------------------------
	
		//----------------------------------
		//  borderMetrics
		//----------------------------------
	
		/**
		 *  @private
		 *  Storage for the borderMetrics property.
		 */
		private var _borderMetrics:EdgeMetrics = new EdgeMetrics(1, 1, 1, 1);
	
		/**
		 *  @private
		 */
		override public function get borderMetrics():EdgeMetrics
		{
			return _borderMetrics;
		}
	
		//----------------------------------
		//  measuredWidth
		//----------------------------------
		
		/**
		 *  @private
		 */
		override public function get measuredWidth():Number
		{
			return UIComponent.DEFAULT_MEASURED_MIN_WIDTH;
		}
		
		//----------------------------------
		//  measuredHeight
		//----------------------------------
	
		/**
		 *  @private
		 */
		override public function get measuredHeight():Number
		{
			return UIComponent.DEFAULT_MEASURED_MIN_HEIGHT;
		}
	
		//--------------------------------------------------------------------------
		//
		//  Overridden methods
		//
		//--------------------------------------------------------------------------
		
		/**
		 *  @private
		 */
		override protected function updateDisplayList(w:Number, h:Number):void
		{
			super.updateDisplayList(w, h);
	
			// User-defined styles.
			var bevel:Boolean = false;
			var borderColor:uint = 0x009bdb;
			var cornerRadius:Number = 0;
			var fillAlphas:Array = [1,1];
			var fillColors:Array = [0x009bdb, 0x009bdb];//getStyle("fillColors");
	        styleManager.getColorNames(fillColors);
			var themeColor:uint = 0x009bdb;
	
			
			var borderColorDrk1:uint =0x009bdb;
				//ColorUtil.adjustBrightness2(borderColor, -25);
			
			var cr:Number = Math.max(0, cornerRadius);
			var cr1:Number = Math.max(0, cornerRadius);
			
	   		var upFillColors:Array = [ fillColors[0], fillColors[1] ];
	   		
			var upFillAlphas:Array = [ fillAlphas[0], fillAlphas[1] ];
													
			graphics.clear();
	
			// button border/edge
			drawRoundRect(
				0, 0, w, h, cr,
				[ borderColor, borderColorDrk1 ], 1,
				verticalGradientMatrix(0, 0, w, h),
				GradientType.LINEAR, null, 
				{ x: 1, y: 1, w: w - 2, h: h - 2, r: cr1 }); 
	
			// button fill
			drawRoundRect(
				1, 1, w - 2, h - 2, cr1,
				upFillColors, upFillAlphas,
				verticalGradientMatrix(1, 1, w - 2, h - 2)); 
		}
	}

}
