package com.dadrimon2.spinningmaster.controller.constant
{
	/**
	 * Notification constants for the 'App' (View and Controller tiers)
	 * 
	 * This could simply be a list of constants, but the approach used 
	 * here is to create a private vocabulary of verbs and nouns which 
	 * are then combined to create the public constants used by the app.
	 * 
	 * This yields clear and unique protocol for notifcation names:
	 * i.e. "StoryArchitect/Apply/Selection" 
	 * or   "StoryArchitect/Manage/Series"
	 */
	
	public class AppConstants
	{
		// VOCABULARY COMPONENTS
		private static const NAME:String 			= "SpinningMaster";
		private static const SEP:String 			= "/";
		private static const SA:String 				= NAME + SEP;
		
		// VERBS
		private static const MANAGE:String 			= SA + "Manage"  + SEP;
		private static const ADD:String 			= SA + "Add" 	 + SEP;
		private static const EDIT:String 			= SA + "Edit" 	 + SEP;
		private static const DELETE:String 			= SA + "Delete"  + SEP;
		private static const REMOVE:String 			= SA + "Remove"  + SEP;
		private static const APPLY:String 			= SA + "Apply"   + SEP;
		private static const SHOW:String 			= SA + "Show"    + SEP;
		private static const DISCARD:String 		= SA + "Discard" + SEP;
		private static const EXIT:String 			= SA + "Exit" 	 + SEP;
		private static const CLOSE:String 			= SA + "Close"   + SEP;
		private static const MOVE:String 			= SA + "Move"    + SEP;
		private static const CREATE:String 			= SA + "Create"    + SEP;
		private static const TOGGLE:String 			= SA + "Toggle"    + SEP;
		private static const NEW:String 			= SA + "New"    + SEP;
		private static const LOAD:String 			= SA + "Load"    + SEP;
		private static const UPDATE:String 			= SA + "Update"    + SEP;
		private static const TEST:String 			= SA + "Test"    + SEP;
		private static const INCREMENT:String 		= SA + "Increment"    + SEP;
		private static const PLAY:String	 		= SA + "Play"    + SEP;
		private static const PAUSE:String	 		= SA + "Pause"    + SEP;
		private static const STOP	:String	 		= SA + "Stop"    + SEP;
		private static const SELECT	:String	 		= SA + "Select"    + SEP;
		private static const SAVE	:String	 		= SA + "Save"    + SEP;
		private static const SET	:String	 		= SA + "Set"    + SEP;
		private static const COMPLETE:String	 	= SA + "Complete"    + SEP;
		private static const BROWSE:String	 		= SA + "Browse"    + SEP;
		private static const LOADED:String	 		= SA + "Loaded"    + SEP;
		private static const CHANGE:String	 		= SA + "Change"    + SEP;
		private static const SEEK:String	 		= SA + "Seek"    + SEP;
		private static const CHECK:String	 		= SA + "Check"    + SEP;
		
		// NOUNS		
		private static const SERIES:String 	    	= "Series";
		private static const EPISODE:String 	    = "Episode";
		private static const STORY:String 	    	= "Story";
		private static const CAST:String 	    	= "Cast";
		private static const MILIEU:String 	    	= "Milieu";
		private static const CHARACTER:String 		= "Character";
		private static const SETTING:String 		= "Setting";
		private static const NOTE:String 			= "Note";
		private static const SELECTION:String 		= "Selection";
		private static const ITEM:String 			= "Item";
		private static const EDITOR:String 			= "Editor";
		private static const CHOOSER:String 		= "Chooser";
		private static const CHANGES:String 		= "Changes";
		private static const APP:String 			= "App";
		private static const WINDOW:String 			= "Window";
		private static const WORKBENCH:String 		= "Workbench";
		private static const WELCOME:String 		= "Welcome";
		private static const SESSION:String			= "Session";
		private static const PLAYER:String			= "Player";
		private static const MODE:String			= "Mode";
		private static const TRACK:String			= "Track";
		private static const SONG:String			= "Song";
		private static const INDEX:String			= "Index";
		private static const EDITION:String			= "Edition";
		private static const DATA:String			= "Data";
		private static const MUSIC:String			= "Music";
		private static const BAR:String				= "Bar";
		private static const VIDEO:String			= "Video";
		private static const SOURCE:String			= "Source";
		private static const TIME:String			= "Time";
		private static const MEDIA:String			= "Media";
		private static const FULLSCREENMODE:String	= "FullScreenMode";

		// NOTIFICATION NAMES
		public static const STARTUP:String 					= SA + "Startup";
		public static const SHOW_WORKBENCH:String			= SHOW + WORKBENCH;
		public static const SHOW_WELCOME:String				= SHOW + WELCOME;
		public static const SET_WORKBENCH_MODE:String		= TOGGLE + WORKBENCH + MODE;
		public static const LOAD_TRACK:String				= LOAD + TRACK;
		public static const BROWSE_LOAD_TRACK:String		= BROWSE + LOAD + TRACK;
		public static const TRACK_LOADED:String				= TRACK + LOADED;
		
		public static const NEW_SESSION:String			= NEW + SESSION;
		public static const CLOSE_SESSION:String		= CLOSE + SESSION;
		public static const PLAY_SESSION:String			= PLAY + SESSION;
		public static const PAUSE_SESSION:String		= PAUSE + SESSION;
		public static const SEEK_SESSION:String			= SEEK + SESSION;
		public static const STOP_SESSION:String			= STOP + SESSION;
		public static const SAVE_SESSION:String			= SAVE + SESSION;
		public static const LOAD_SESSION:String			= LOAD + SESSION;
		
		public static const UPDATE_SONG_INDEX:String	= UPDATE + SONG + INDEX;
		public static const UPDATE_SONG_DATA:String		= UPDATE + SONG + DATA;
		public static const SELECT_SONG_INDEX:String	= SELECT + SONG + INDEX
		public static const INCREMENT_MUSIC_BAR:String	= INCREMENT + MUSIC + BAR;
		public static const ADD_SONG:String				= ADD + SONG;
		public static const DELETE_SONG:String			= DELETE + SONG;
		public static const SELECT_SONG:String			= SELECT + SONG;
		public static const SAVE_SONG:String			= SAVE + SONG;
		public static const CHECK_NEW_SONG:String		= CHECK + NEW + SONG;
		
		public static const CHANGE_VIDEO_SOURCE:String	= CHANGE + VIDEO + SOURCE;
		public static const CHANGE_VIDEO_TIME:String	= CHANGE + VIDEO + TIME;
		public static const LOAD_MEDIA:String			= LOAD + MEDIA;
		
		public static const CLOSE_APP:String			= CLOSE + APP;
		public static const SET_FULLSCREENMODE:String	= SET + FULLSCREENMODE;
		public static const TOGGLE_FULLSCREENMODE:String= TOGGLE + FULLSCREENMODE;
		
		public static const SESSION_SAVE_COMPLETE:String= SESSION + SAVE + COMPLETE;
		
		public static const TEST_SONG_EDITION:String	= TEST + SONG + EDITION;
	}
}