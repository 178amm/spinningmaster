package com.dadrimon2.spinningmaster.controller.command.media
{
	import com.dadrimon2.spinningmaster.controller.constant.AppConstants;
	import com.dadrimon2.spinningmaster.view.context.SelectionContext;
	
	import flash.events.Event;
	import flash.filesystem.File;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	public class BrowseForMediaCommand extends SimpleCommand
	{
		private var selectionContext:SelectionContext;
		private var auxFile:File;
		private var songToModifyIndex:uint;
		
		override public function execute(note:INotification):void
		{		
			// Get the SelectionContext
			var scProxy:IProxy = facade.retrieveProxy(SelectionContext.NAME);
			this.selectionContext = SelectionContext(scProxy.getData());

			songToModifyIndex = uint(note.getBody());
			
			//Set the listener to the auxfile 
			this.auxFile = new File();
			this.auxFile.browseForOpen("Seleccione un archivo");
			this.auxFile.addEventListener(Event.SELECT, handleFileSelection);		
		}
		
		private function handleFileSelection(e:Event):void
		{
			//Set the url at the context
			selectionContext.sessionVO.songs[songToModifyIndex].media = this.auxFile.url;

			//TODO check if is an image or a video, and check if exists
			this.sendNotification(AppConstants.CHANGE_VIDEO_SOURCE, this.auxFile.url);
		}
			
	}
}