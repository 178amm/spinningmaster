package com.dadrimon2.spinningmaster.controller.command.session
{
	import com.dadrimon2.spinningmaster.controller.constant.AppConstants;
	import com.dadrimon2.spinningmaster.model.proxy.TrackProxy;
	import com.dadrimon2.spinningmaster.model.proxy.WorkbenchTimerProxy;
	import com.dadrimon2.spinningmaster.model.vo.SongVO;
	import com.dadrimon2.spinningmaster.view.context.SelectionContext;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.command.SimpleCommand;

	
	public class SeekSessionCommand extends SimpleCommand
	{
		private var wbtProxy:WorkbenchTimerProxy;
		private var tkProxy:TrackProxy;
		private var sc:SelectionContext;
		
		override public function execute(note:INotification):void
		{
			//Pauses the session
			sendNotification(AppConstants.PAUSE_SESSION);
			
			//Get the object time it wants to go
			var seekTime:Number = Number(note.getBody());
			
			//Gets the needed proxies
			wbtProxy = WorkbenchTimerProxy(facade.retrieveProxy(WorkbenchTimerProxy.NAME));
			tkProxy = TrackProxy(facade.retrieveProxy(TrackProxy.NAME));
			
			// Get the SelectionContext
			var scProxy:IProxy = facade.retrieveProxy(SelectionContext.NAME);
			sc = SelectionContext(scProxy.getData());
			
			//Calculates the time for updating the video time
			try
			{
				var auxSong:SongVO = sc.sessionVO.getSongBySongTime(seekTime);
				var videoTime:Number = seekTime - sc.sessionVO.getSongTimeBySong(auxSong);
			}
			catch (E:Error)
			{
				var videotime:Number = 0;
			}
			
			//Updates the timer
			wbtProxy.trackTimerElapsed = seekTime;
			
			//Prepares the object for updating the view
			var reached:Object = new Object();
			reached.currentValue = seekTime;
			
			//Update the view
			sendNotification(WorkbenchTimerProxy.VIEW_UPDATER_TICK, reached);
			sendNotification(AppConstants.CHANGE_VIDEO_TIME, videoTime);
		}
	}
}