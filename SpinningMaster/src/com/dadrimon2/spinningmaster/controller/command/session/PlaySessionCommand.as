package com.dadrimon2.spinningmaster.controller.command.session
{
	import com.dadrimon2.spinningmaster.controller.constant.AppConstants;
	import com.dadrimon2.spinningmaster.model.proxy.TrackProxy;
	import com.dadrimon2.spinningmaster.model.proxy.WorkbenchTimerProxy;
	import com.dadrimon2.spinningmaster.view.context.SelectionContext;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.command.SimpleCommand;

	
	public class PlaySessionCommand extends SimpleCommand
	{
		private var wbtProxy:WorkbenchTimerProxy;
		private var tkProxy:TrackProxy;
		private var sc:SelectionContext;
		
		override public function execute(note:INotification):void
		{
			//Gets the needed proxies
			wbtProxy = WorkbenchTimerProxy(facade.retrieveProxy(WorkbenchTimerProxy.NAME));
			tkProxy = TrackProxy(facade.retrieveProxy(TrackProxy.NAME));
			
			// Get the SelectionContext
			var scProxy:IProxy = facade.retrieveProxy(SelectionContext.NAME);
			sc = SelectionContext(scProxy.getData());
			
			//Starts to play the song and the timer
			sc.playSong(tkProxy.trackSound, wbtProxy.trackTimerElapsed); 
			wbtProxy.start();
		}
	}
}