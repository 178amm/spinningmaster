package com.dadrimon2.spinningmaster.controller.command.session
{
	import com.dadrimon2.spinningmaster.model.proxy.SessionProxy;
	import com.dadrimon2.spinningmaster.view.context.SelectionContext;
	
	import flash.events.Event;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	public class SaveSessionCommand extends SimpleCommand
	{
		private var selectionContext:SelectionContext;
		private var sessionProxy:SessionProxy;
		private var auxFunction:Function;
		
		override public function execute(note:INotification):void
		{
			//Get the body func.
			auxFunction = (note.getBody() as Function);
			
			// Get the SelectionContext
			var scProxy:IProxy = facade.retrieveProxy(SelectionContext.NAME);
			this.selectionContext = SelectionContext(scProxy.getData());
			
			//Get the needed proxies
			this.sessionProxy = SessionProxy(facade.retrieveProxy(SessionProxy.NAME));
			
			//Check if the file has already been saved
			if (sessionProxy.fileLoaded)
			{
				this.save();
			}
			else
			{
				sessionProxy.file.addEventListener(Event.SELECT, handleFileSelection);	
				sessionProxy.file.browseForSave("Seleccione donde guardar");	
			}
		}
		
		private function handleFileSelection(e:Event):void
		{
			this.save();
		}
		
		
		private function save():void
		{
			sessionProxy.saveSession(this.selectionContext.sessionVO);
			if (this.auxFunction != null)
			{
				this.auxFunction();
			}
		}

	}
}