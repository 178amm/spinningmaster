package com.dadrimon2.spinningmaster.controller.command.session
{
	import com.dadrimon2.spinningmaster.controller.constant.AppConstants;
	import com.dadrimon2.spinningmaster.model.proxy.TrackProxy;
	import com.dadrimon2.spinningmaster.model.proxy.WorkbenchTimerProxy;
	import com.dadrimon2.spinningmaster.model.vo.SessionVO;
	import com.dadrimon2.spinningmaster.model.vo.SongVO;
	import com.dadrimon2.spinningmaster.model.vo.TrackVO;
	import com.dadrimon2.spinningmaster.view.component.workbench.Workbench;
	import com.dadrimon2.spinningmaster.view.context.SelectionContext;
	import com.dadrimon2.spinningmaster.view.popup.event.PopupActionEvent;
	import com.dadrimon2.spinningmaster.view.popup.request.PopupRequest;
	
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	public class LoadSessionCommand extends SimpleCommand
	{
		private var selectionContext:SelectionContext;
		private var auxFile:File;
		
		override public function execute(note:INotification):void
		{
			//Uses the currently app mode to see if theres other session currently being edited.
			var appMode:String = (note.getBody() as String);
			
			//If theres a session opened, show popup to save it
			if (appMode == SpinningMaster.MODE_WORKBENCH)
			{
				// Request the popup
				var request:PopupRequest = new PopupRequest(handleConfirmPopupNotification, this);
				sendNotification(PopupRequest.SAVE_SESSION_POPUP, request);
			}
			else
			{
				//Launches the file browser
				this.loadFromFile();
			}
			
		}
		
		private function loadFromFile():void
		{
			// Get the SelectionContext
			var scProxy:IProxy = facade.retrieveProxy(SelectionContext.NAME);
			this.selectionContext = SelectionContext(scProxy.getData());
			
			//Set the listener to the auxfile 
			this.auxFile = new File();
			this.auxFile.browseForOpen("Seleccione un archivo");
			this.auxFile.addEventListener(Event.SELECT, handleFileSelection);
		}
		
		private function handleFileSelection(e:Event):void
		{
			//@TODO mover al proxy
			var stream:FileStream = new FileStream();
			stream.open(this.auxFile, FileMode.READ);	
			var xml:XML = XML(stream.readUTFBytes(stream.bytesAvailable));
			stream.close();
			
			//Asign the loaded xml file to the context one
			this.selectionContext.sessionVO = new SessionVO(xml);
			
			//Tries to find the song attached in the xml file
			this.auxFile = new File(this.selectionContext.sessionVO.track.fullName);
			
			if (!this.auxFile.exists)
			{
				// Request the popup
				var request:PopupRequest = new PopupRequest(handleConfirmPopupNotification2, this);
				sendNotification(PopupRequest.TRACK_CONFLICT_POPUP, request);
			}
			else
			{
				sendNotification(AppConstants.LOAD_TRACK, this.auxFile);
			}
			
			//View update
			sendNotification(AppConstants.SET_WORKBENCH_MODE, Workbench.MODE_EDITOR);
			sendNotification(AppConstants.SHOW_WORKBENCH);
			sendNotification(AppConstants.UPDATE_SONG_INDEX, this.selectionContext.sessionVO);
		}
		
		
		//Callback called when the user clicks on the "Save" popup
		private function handleConfirmPopupNotification(actionNote:INotification):void
		{
			var event:PopupActionEvent = PopupActionEvent(actionNote.getBody());
			
			switch (actionNote.getName()) 
			{
				case PopupActionEvent.OK:
					sendNotification(AppConstants.SAVE_SESSION, this.loadFromFile);
					break;
				case PopupActionEvent.CANCEL:
					//Launches the file browser
					this.loadFromFile();
					break;
			}
		}
		
		//Callback called when the user clicks on the "TrackConflict" popup
		private function handleConfirmPopupNotification2(actionNote:INotification):void
		{
			var event:PopupActionEvent = PopupActionEvent(actionNote.getBody());
			
			switch (actionNote.getName()) 
			{
				case PopupActionEvent.OK:
					sendNotification(AppConstants.BROWSE_LOAD_TRACK);
					break;
				case PopupActionEvent.CANCEL:
					break;
			}
		}
		
	}
}