package com.dadrimon2.spinningmaster.controller.command.session
{
	import com.dadrimon2.spinningmaster.controller.constant.AppConstants;
	import com.dadrimon2.spinningmaster.model.proxy.SessionProxy;
	import com.dadrimon2.spinningmaster.model.vo.SessionVO;
	import com.dadrimon2.spinningmaster.model.vo.ValueObject;
	import com.dadrimon2.spinningmaster.view.component.workbench.Workbench;
	import com.dadrimon2.spinningmaster.view.context.SelectionContext;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	public class NewSessionCommand extends SimpleCommand
	{
		override public function execute(note:INotification):void
		{
			//Uses the currently app mode to see if theres other session currently being edited.
			var appMode:String = (note.getBody() as String);

			//If there's no session opened
			if (appMode == SpinningMaster.MODE_WELCOME)
			{
				//New VO
				var session:SessionVO = new SessionVO();
				//Cache it
				var scProxy:IProxy = facade.retrieveProxy(SelectionContext.NAME);
				var selectionContext:SelectionContext = SelectionContext(scProxy.getData());	
				selectionContext.sessionVO = session;
				
				//Notify the view to init edition mode
				sendNotification(AppConstants.SET_WORKBENCH_MODE, Workbench.MODE_EDITOR);
			}
			//Else, session opened, show prompt to save it
			else
			{
				//@TODO controlarlo
			}
			
			//Notify the view
			sendNotification(AppConstants.SHOW_WORKBENCH);
		}
	}
}