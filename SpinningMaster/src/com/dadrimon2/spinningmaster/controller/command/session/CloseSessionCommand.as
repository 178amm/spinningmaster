package com.dadrimon2.spinningmaster.controller.command.session
{
	import com.dadrimon2.spinningmaster.controller.constant.AppConstants;
	import com.dadrimon2.spinningmaster.model.proxy.SessionProxy;
	import com.dadrimon2.spinningmaster.model.vo.SessionVO;
	import com.dadrimon2.spinningmaster.model.vo.ValueObject;
	import com.dadrimon2.spinningmaster.view.component.workbench.Workbench;
	import com.dadrimon2.spinningmaster.view.context.SelectionContext;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	public class CloseSessionCommand extends SimpleCommand
	{
		private var selectionContext:SelectionContext;
		private var sessionProxy:SessionProxy;
		
		override public function execute(note:INotification):void
		{
			//@TODO mostrar prompt guardar sesion
			sendNotification(AppConstants.SAVE_SESSION, this.afterSave);
		}
		
		private function afterSave():void
		{
			//Reset the workbench
			var scProxy:IProxy = facade.retrieveProxy(SelectionContext.NAME);
			this.selectionContext = SelectionContext(scProxy.getData());
			this.selectionContext.reset();
			sendNotification(AppConstants.UPDATE_SONG_INDEX, this.selectionContext.sessionVO);
			
			//Notify the view
			sendNotification(AppConstants.SHOW_WELCOME);
			//test
		}
	}
}