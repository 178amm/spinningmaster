package com.dadrimon2.spinningmaster.controller.command.song
{

	import com.dadrimon2.spinningmaster.controller.constant.AppConstants;
	import com.dadrimon2.spinningmaster.view.context.SelectionContext;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	public class SelectSongCommand extends SimpleCommand
	{
		private var selectionContext:SelectionContext;
		private var songSelectedIndex:uint;
		
		override public function execute(note:INotification):void
		{
			// Get the SelectionContext
			var scProxy:IProxy = facade.retrieveProxy(SelectionContext.NAME);
			this.selectionContext = SelectionContext(scProxy.getData());
			
			songSelectedIndex = uint(note.getBody());
			
			var media = this.selectionContext.sessionVO.songs[songSelectedIndex].media
			var nuc:String = null;

			// Notify the song data view
			sendNotification(AppConstants.UPDATE_SONG_DATA, this.selectionContext.sessionVO.songs[songSelectedIndex]);
		}
	}
}