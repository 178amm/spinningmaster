package com.dadrimon2.spinningmaster.controller.command.song
{

	import com.dadrimon2.spinningmaster.controller.constant.AppConstants;
	import com.dadrimon2.spinningmaster.model.vo.SongVO;
	import com.dadrimon2.spinningmaster.view.context.SelectionContext;
	import com.dadrimon2.spinningmaster.view.popup.event.PopupActionEvent;
	import com.dadrimon2.spinningmaster.view.popup.request.PopupRequest;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	public class SaveSongCommand extends SimpleCommand
	{
		private var selectionContext:SelectionContext;
		private var songSelectedIndex:uint;
		private var song:SongVO;
		
		override public function execute(note:INotification):void
		{
			// Get the SelectionContext
			var scProxy:IProxy = facade.retrieveProxy(SelectionContext.NAME);
			this.selectionContext = SelectionContext(scProxy.getData());
			
			var eData:Object = note.getBody();
			
			
			//@TODO, manage the part & technique setter
			//this.currentSongID.text = "Cancion " + this.songList.selectedItem getnumber
			this.selectionContext.sessionVO.songs[eData.index].songName = eData.songName; 
			
			this.selectionContext.sessionVO.songs[eData.index].intensity = eData.intensity; 
			this.selectionContext.sessionVO.songs[eData.index].part = eData.partIndex;
			this.selectionContext.sessionVO.songs[eData.index].technique = eData.techniqueIndex;
			this.selectionContext.sessionVO.songs[eData.index].rpm = eData.rpm; 
			this.selectionContext.sessionVO.songs[eData.index].heightReached = eData.heightReached; 
			this.selectionContext.sessionVO.songs[eData.index].lengthTime = eData.length;
			
			trace("Save");
		}
	}
}