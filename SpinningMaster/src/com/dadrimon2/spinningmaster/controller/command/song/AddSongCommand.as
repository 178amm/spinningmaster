package com.dadrimon2.spinningmaster.controller.command.song
{

	import com.dadrimon2.spinningmaster.controller.constant.AppConstants;
	import com.dadrimon2.spinningmaster.model.vo.SongVO;
	import com.dadrimon2.spinningmaster.view.context.SelectionContext;
	import com.dadrimon2.spinningmaster.view.popup.event.PopupActionEvent;
	import com.dadrimon2.spinningmaster.view.popup.request.PopupRequest;
	import com.dadrimon2.utils.Parser;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	public class AddSongCommand extends SimpleCommand
	{
		private var selectionContext:SelectionContext;
		
		override public function execute(note:INotification):void
		{
			// Get the SelectionContext
			var scProxy:IProxy = facade.retrieveProxy(SelectionContext.NAME);
			this.selectionContext = SelectionContext(scProxy.getData());
			
			//Gets the current Music player slider cursor position
			var currentTimePosition:Number = Number(note.getBody());
			trace ("Current time position: " + currentTimePosition);
			
			//Calculates the recomended song length
			var recommendedLength:Date = new Date(0,0,0,0,0,0,0);
			var songIndex:Vector.<Number> = this.selectionContext.sessionVO.songTimeIndex;
			
			//If the slider throws a right position, else the recommended lenght is 0
			if (!isNaN(currentTimePosition))
			{
				//Gives a recommended song lenght based on the current cursor position
				if (songIndex.length>0)
				{
					trace("Enter");
					trace("Song index length: " + songIndex[songIndex.length-1]);
					recommendedLength = Parser.millisecondsToDate((currentTimePosition 
																	- songIndex[songIndex.length-1] 
																	- Number(this.selectionContext.sessionVO.songs[songIndex.length-1].lengthTime)).toString());
				}
				else
				{
					recommendedLength = Parser.millisecondsToDate(currentTimePosition.toString());
				}
			}
			
			trace ("RecommendedLength: " + recommendedLength.minutes + ", " + recommendedLength.seconds);
			
			// Request the popup
			var request:PopupRequest = new PopupRequest(handleConfirmPopupNotification, this);
			request.data.recommendedLength = recommendedLength;
			
			sendNotification(PopupRequest.ADD_SONG_POPUP, request);
		}
		
		private function handleConfirmPopupNotification(actionNote:INotification):void
		{
			var event:PopupActionEvent = PopupActionEvent(actionNote.getBody());
			
			switch (actionNote.getName()) 
			{
				case PopupActionEvent.OK:
					selectionContext.sessionVO.addSong(SongVO(event.data));
					sendNotification(AppConstants.UPDATE_SONG_INDEX, selectionContext.sessionVO);
					sendNotification(AppConstants.SELECT_SONG_INDEX, selectionContext.sessionVO.songs.length-1);
					sendNotification(AppConstants.UPDATE_SONG_DATA, SongVO(event.data));
					break;
				
				case PopupActionEvent.CANCEL:
					break;
			}
		}	
	}
}