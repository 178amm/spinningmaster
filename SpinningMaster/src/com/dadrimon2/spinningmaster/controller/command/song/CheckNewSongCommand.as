package com.dadrimon2.spinningmaster.controller.command.song
{

	import com.dadrimon2.spinningmaster.controller.constant.AppConstants;
	import com.dadrimon2.spinningmaster.model.vo.SongVO;
	import com.dadrimon2.spinningmaster.view.context.SelectionContext;
	import com.dadrimon2.spinningmaster.view.popup.event.PopupActionEvent;
	import com.dadrimon2.spinningmaster.view.popup.request.PopupRequest;
	import com.dadrimon2.utils.Parser;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	public class CheckNewSongCommand extends SimpleCommand
	{
		private var selectionContext:SelectionContext;
		
		override public function execute(note:INotification):void
		{
			// Get the SelectionContext
			var scProxy:IProxy = facade.retrieveProxy(SelectionContext.NAME);
			this.selectionContext = SelectionContext(scProxy.getData());
			
			try
			{
				//Get the current song
				var lastValue:Number = (note.getBody().lastValue);
				trace ("Last value: " + lastValue);
				//Get the current time
				var currentValue:Number = (note.getBody().currentValue);
				trace ("CurrentValue: " + currentValue);
			}
			catch (e:Error)
			{
				trace("Error retrieving song and time");
			}
			
			//Check if the song has changed, if so, switch the song
			var lastSong:SongVO = this.selectionContext.sessionVO.getSongBySongTime(lastValue);
			var newSong:SongVO = this.selectionContext.sessionVO.getSongBySongTime(currentValue);
			
			if (lastSong != null && newSong != null && !lastSong.equals(newSong))
			{
				for (var i:uint=0; i < this.selectionContext.sessionVO.songs.length; i++)
				{
					if (this.selectionContext.sessionVO.songs[i].equals(newSong))
					{
						sendNotification(AppConstants.SELECT_SONG, i);
					}
				}
			}
		}

	}
}