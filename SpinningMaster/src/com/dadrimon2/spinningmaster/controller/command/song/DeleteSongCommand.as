package com.dadrimon2.spinningmaster.controller.command.song
{

	import com.dadrimon2.spinningmaster.controller.constant.AppConstants;
	import com.dadrimon2.spinningmaster.model.vo.SongVO;
	import com.dadrimon2.spinningmaster.view.context.SelectionContext;
	import com.dadrimon2.spinningmaster.view.popup.event.PopupActionEvent;
	import com.dadrimon2.spinningmaster.view.popup.request.PopupRequest;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	public class DeleteSongCommand extends SimpleCommand
	{
		private var selectionContext:SelectionContext;
		private var songToDeleteIndex:uint;
		
		override public function execute(note:INotification):void
		{
			// Get the SelectionContext
			var scProxy:IProxy = facade.retrieveProxy(SelectionContext.NAME);
			this.selectionContext = SelectionContext(scProxy.getData());
			
			songToDeleteIndex = uint(note.getBody());
			
			// Request the popup
			var request:PopupRequest = new PopupRequest(handleConfirmPopupNotification, this);
			
			sendNotification(PopupRequest.DELETE_SONG_POPUP, request);
		}
		
		private function handleConfirmPopupNotification(actionNote:INotification):void
		{
			var event:PopupActionEvent = PopupActionEvent(actionNote.getBody());
			
			switch (actionNote.getName()) 
			{
				case PopupActionEvent.OK:
					trace("Song to delete: " + this.songToDeleteIndex);
					selectionContext.sessionVO.removeSong(this.selectionContext.sessionVO.songs[songToDeleteIndex]);
					sendNotification(AppConstants.UPDATE_SONG_INDEX, selectionContext.sessionVO);
					break;
				
				case PopupActionEvent.CANCEL:
					break;
			}
		}	
	}
}