package com.dadrimon2.spinningmaster.controller.command.startup
{
	import com.dadrimon2.spinningmaster.view.context.SelectionContext;
	import com.dadrimon2.spinningmaster.view.mediator.ApplicationMediator;
	import com.dadrimon2.spinningmaster.view.mediator.WorkbenchMediator;
	import com.dadrimon2.spinningmaster.view.popup.mediator.AboutPopupMediator;
	import com.dadrimon2.spinningmaster.view.popup.mediator.AddSongPopupMediator;
	import com.dadrimon2.spinningmaster.view.popup.mediator.DeleteSongPopupMediator;
	import com.dadrimon2.spinningmaster.view.popup.mediator.SaveSessionPopupMediator;
	import com.dadrimon2.spinningmaster.view.popup.mediator.TrackConflictPopupMediator;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	import org.puremvc.as3.patterns.proxy.Proxy;
	
	public class PrepareViewCommand extends SimpleCommand
	{
		override public function execute( note:INotification):void
		{
			// Get the application from the note body
			var app:SpinningMaster = SpinningMaster(note.getBody());
			
			var selectionContext:SelectionContext = new SelectionContext();
			var scProxy:Proxy = new Proxy( SelectionContext.NAME, 
				selectionContext ) 
			facade.registerProxy(scProxy); 
			
			// Mediate the initial view components
			facade.registerMediator(new ApplicationMediator(app));
			facade.registerMediator(new WorkbenchMediator(app.workbench));

			// Register the popup mediators
			facade.registerMediator( new AboutPopupMediator() );
			facade.registerMediator( new AddSongPopupMediator() );
			facade.registerMediator( new DeleteSongPopupMediator() );
			facade.registerMediator( new SaveSessionPopupMediator() );
			facade.registerMediator( new TrackConflictPopupMediator() );
		}
			
	}
}