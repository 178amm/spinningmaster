package com.dadrimon2.spinningmaster.controller.command.startup
{
	import com.dadrimon2.spinningmaster.controller.command.session.*;
	import com.dadrimon2.spinningmaster.controller.command.song.*;
	import com.dadrimon2.spinningmaster.controller.command.test.*;
	import com.dadrimon2.spinningmaster.controller.command.track.*;
	import com.dadrimon2.spinningmaster.controller.command.media.*;
	import com.dadrimon2.spinningmaster.controller.command.app.*;
	import com.dadrimon2.spinningmaster.controller.constant.AppConstants;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	public class PrepareControllerCommand extends SimpleCommand
	{
		override public function execute(note:INotification):void
		{
			facade.registerCommand(AppConstants.NEW_SESSION, NewSessionCommand);
			facade.registerCommand(AppConstants.CLOSE_SESSION, CloseSessionCommand)
			facade.registerCommand(AppConstants.PLAY_SESSION, PlaySessionCommand);
			facade.registerCommand(AppConstants.PAUSE_SESSION, PauseSessionCommand);
			facade.registerCommand(AppConstants.STOP_SESSION, StopSessionCommand);
			facade.registerCommand(AppConstants.SAVE_SESSION, SaveSessionCommand);
			facade.registerCommand(AppConstants.LOAD_SESSION, LoadSessionCommand);
			facade.registerCommand(AppConstants.SEEK_SESSION, SeekSessionCommand);
				
			facade.registerCommand(AppConstants.ADD_SONG, AddSongCommand);
			facade.registerCommand(AppConstants.DELETE_SONG, DeleteSongCommand);
			facade.registerCommand(AppConstants.SELECT_SONG, SelectSongCommand);
			facade.registerCommand(AppConstants.SAVE_SONG, SaveSongCommand);
			facade.registerCommand(AppConstants.CHECK_NEW_SONG, CheckNewSongCommand);
			
			facade.registerCommand(AppConstants.LOAD_TRACK, LoadTrackCommand);
			facade.registerCommand(AppConstants.BROWSE_LOAD_TRACK, BrowseForLoadTrackCommand);
			
			facade.registerCommand(AppConstants.LOAD_MEDIA, BrowseForMediaCommand);
			
			facade.registerCommand(AppConstants.CLOSE_APP, CloseAppCommand);
			facade.registerCommand(AppConstants.TOGGLE_FULLSCREENMODE, ToggleFullScreenModeCommand);
			
			facade.registerCommand(AppConstants.TEST_SONG_EDITION, TestSongEditionCommand);
		}
	}
}