package com.dadrimon2.spinningmaster.controller.command.startup
{
	import org.puremvc.as3.patterns.command.MacroCommand;
	
	public class StartupCommand extends MacroCommand
	{
		override protected function initializeMacroCommand():void
		{
			addSubCommand(PrepareControllerCommand);
			addSubCommand(PrepareModelCommand);
			addSubCommand(PrepareViewCommand);
			
			trace ("Initialize startup macrocommand");
		}
	}
}