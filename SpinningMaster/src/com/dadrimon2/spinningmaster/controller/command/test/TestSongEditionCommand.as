package com.dadrimon2.spinningmaster.controller.command.test
{
	import com.dadrimon2.spinningmaster.controller.constant.AppConstants;
	import com.dadrimon2.spinningmaster.model.enum.*;
	import com.dadrimon2.spinningmaster.model.vo.SessionVO;
	import com.dadrimon2.spinningmaster.model.vo.SongVO;
	import com.dadrimon2.spinningmaster.model.vo.TimeDataVO;
	import com.dadrimon2.spinningmaster.model.vo.ValueObject;
	import com.dadrimon2.spinningmaster.view.context.SelectionContext;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	
	public class TestSongEditionCommand extends SimpleCommand
	{
		override public function execute(note:INotification):void
		{
			var songName0:String = "Cancion1 con titulo 1";
			var part0:String = PartEnum.ACONDICIONAMIENTO;
			var technique0:String = TechniqueEnum.LLANO;
			var intensity0:String = "55";
			var rpm0:String = "40";
			var heightReached0:String = "35";
			var timeData0:String = "10000";
			
			var song0:SongVO = new SongVO();
			song0.songName	= songName0;
			song0.part 		= part0;
			song0.technique = technique0;
			song0.intensity = intensity0;
			song0.rpm 		= rpm0;
			song0.heightReached = heightReached0;
			song0.lengthTime = timeData0;
			
			var songName1:String = "Cancion 2 Justin Timberlake - Senorita";
			var part1:String = PartEnum.ACONDICIONAMIENTO;
			var technique1:String = TechniqueEnum.LLANO;
			var intensity1:String = "45";
			var rpm1:String = "95";
			var heightReached1:String = "35";
			var timeData1:String = "11000";
			
			var song1:SongVO = new SongVO();
			song1.songName	= songName1;
			song1.part 		= part1;
			song1.technique = technique1;
			song1.intensity = intensity1;
			song1.rpm 		= rpm1;
			song1.heightReached = heightReached1;
			song1.lengthTime = timeData1;
			
			var songName2:String = "Cancion Test 3 - Testeando";
			var part2:String = PartEnum.ACONDICIONAMIENTO;
			var technique2:String = TechniqueEnum.LLANO;
			var intensity2:String = "45";
			var rpm2:String = "25";
			var heightReached2:String = "80";
			var timeData2:String = "12000";
			
			var song2:SongVO = new SongVO();
			song2.songName	= songName2;
			song2.part 		= part2;
			song2.technique = technique2;
			song2.intensity = intensity2;
			song2.rpm 		= rpm2;
			song2.heightReached = heightReached2;
			song2.lengthTime = timeData2;
			
			var songName3:String = "Cancion 4 Usher test";
			var part3:String = PartEnum.ACONDICIONAMIENTO;
			var technique3:String = TechniqueEnum.LLANO;
			var intensity3:String = "76";
			var rpm3:String = "34";
			var heightReached3:String = "90";
			var timeData3:String = "13000";
			
			var song3:SongVO = new SongVO();
			song3.songName	= songName3;
			song3.part 		= part3;
			song3.technique = technique3;
			song3.intensity = intensity3;
			song3.rpm 		= rpm3;
			song3.heightReached = heightReached3;
			song3.lengthTime = timeData3;
			
			//-----------------------------------------------------------------------
			var sessionVO1:SessionVO = new SessionVO();
			
			var sessionName:String = "Session 1 - My first one";
			
			sessionVO1.sessionName = sessionName;
			
			
			// Get the SelectionContext
			var scProxy:IProxy = facade.retrieveProxy(SelectionContext.NAME);
			var selectionContext:SelectionContext = SelectionContext(scProxy.getData());
			selectionContext.sessionVO.addSong(song0);
			selectionContext.sessionVO.addSong(song1);
			selectionContext.sessionVO.addSong(song2);
			selectionContext.sessionVO.addSong(song3);
			
			//Update the view with this data
			sendNotification(AppConstants.UPDATE_SONG_INDEX, selectionContext.sessionVO);
			

			//sendNotification(AppConstants.UPDATE_SONG_DATA, object);
			
		}
	}
}