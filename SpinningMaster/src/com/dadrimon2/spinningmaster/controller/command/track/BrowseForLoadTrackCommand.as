package com.dadrimon2.spinningmaster.controller.command.track
{
	import com.dadrimon2.spinningmaster.view.context.SelectionContext;
	import com.dadrimon2.spinningmaster.controller.constant.AppConstants;
	
	import flash.events.Event;
	import flash.filesystem.File;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	public class BrowseForLoadTrackCommand extends SimpleCommand
	{
		private var selectionContext:SelectionContext;
		private var auxFile:File;
		
		override public function execute(note:INotification):void
		{
			//Set the listener to the auxfile 
			this.auxFile = new File();
			this.auxFile.browseForOpen("Seleccione un archivo");
			this.auxFile.addEventListener(Event.SELECT, handleFileSelection);		
		}
		
		private function handleFileSelection(e:Event):void
		{
			this.sendNotification(AppConstants.LOAD_TRACK, this.auxFile);
		}
			
	}
}