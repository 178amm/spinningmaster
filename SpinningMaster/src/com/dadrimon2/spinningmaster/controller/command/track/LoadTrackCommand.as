package com.dadrimon2.spinningmaster.controller.command.track
{
	import com.dadrimon2.spinningmaster.model.proxy.TrackProxy;
	import com.dadrimon2.spinningmaster.model.proxy.WorkbenchTimerProxy;
	import com.dadrimon2.spinningmaster.view.context.SelectionContext;
	import com.dadrimon2.spinningmaster.controller.constant.AppConstants;
	
	import flash.filesystem.File;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	public class LoadTrackCommand extends SimpleCommand
	{
		private var selectionContext:SelectionContext;
		private var auxFile:File;
		
		override public function execute(note:INotification):void
		{
			//Get the file from the body
			auxFile = (note.getBody() as File);
			
			//Retrieves the track and context proxy
			var scProxy:IProxy = facade.retrieveProxy(SelectionContext.NAME);
			this.selectionContext = SelectionContext(scProxy.getData());
			
			var trackProxy:TrackProxy = TrackProxy(facade.retrieveProxy(TrackProxy.NAME));
			trackProxy.loadTrackFromFile(this.auxFile, onAssetLoaded);
		}
		
		public function onAssetLoaded():void
		{
			//Set the length of the trackTimer based on the song file length
			var wktProxy:WorkbenchTimerProxy = WorkbenchTimerProxy(facade.retrieveProxy(WorkbenchTimerProxy.NAME));
			var trackProxy:TrackProxy = TrackProxy(facade.retrieveProxy(TrackProxy.NAME));
			
			wktProxy.trackTimerTick = trackProxy.trackSound.length;
			
			//Update the trackVO at the current context
			this.selectionContext.sessionVO.track = trackProxy.loadedTrackVO;
			
			//Notify the view
			this.sendNotification(AppConstants.TRACK_LOADED);
		}
			
	}
}