package com.dadrimon2.spinningmaster.controller.command.app
{
	import com.dadrimon2.spinningmaster.controller.constant.AppConstants;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	public class ToggleFullScreenModeCommand extends SimpleCommand
	{
		override public function execute(note:INotification):void
		{
			//Get the current fullScreenMode
			var fullScreenMode:String = (note.getBody() as String);
			
			trace("FullScreenModeCommand: " + fullScreenMode);
			
			//Switch the mode depending on the current one
			switch (fullScreenMode)
			{
				case (SpinningMaster.FULLSCREENMODE_ON) :
					sendNotification(AppConstants.SET_FULLSCREENMODE, SpinningMaster.FULLSCREENMODE_OFF);
					break;
				case (SpinningMaster.FULLSCREENMODE_OFF) :
					sendNotification(AppConstants.SET_FULLSCREENMODE, SpinningMaster.FULLSCREENMODE_ON);
					break;
			}
		}
		
	}
}