package com.dadrimon2.spinningmaster.controller.command.app
{
	import com.dadrimon2.spinningmaster.controller.constant.AppConstants;
	import com.dadrimon2.spinningmaster.model.proxy.SessionProxy;
	import com.dadrimon2.spinningmaster.model.vo.SessionVO;
	import com.dadrimon2.spinningmaster.model.vo.ValueObject;
	import com.dadrimon2.spinningmaster.view.component.workbench.Workbench;
	import com.dadrimon2.spinningmaster.view.context.SelectionContext;
	
	import org.puremvc.as3.interfaces.INotification;
	import org.puremvc.as3.interfaces.IProxy;
	import org.puremvc.as3.patterns.command.SimpleCommand;
	
	public class CloseAppCommand extends SimpleCommand
	{
		override public function execute(note:INotification):void
		{
			//Uses the currently app mode to see if theres other session currently being edited.
			var appMode:String = (note.getBody() as String);

			//If there's no session opened
			if (appMode == SpinningMaster.MODE_WELCOME)
			{
				this.afterSave();
			}
			//Else, session opened, show prompt to save it
			else
			{
				sendNotification(AppConstants.SAVE_SESSION, this.afterSave);
			}
		}
		
		private function afterSave()
		{
			//Notifies the view to close de app
			this.sendNotification(AppConstants.CLOSE_APP);
		}
		
		
	}
}